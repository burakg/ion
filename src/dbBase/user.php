<?php
/**
 *
 * Class user
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 20.03.2015
 *
 */

namespace burakg\ion\dbBase;

use burakg\ion AS ion;
use burakg\ion\dbBase;
use burakg\ionAdmin AS admin;

require_once(CB_LIB.'ircmaxell/password-compat/lib/password.php');

class user extends dbBase {
	use dbBaseTrait;
	use mediumTrait;

	protected function init(){
		$this->properties->tableName = 'users';
		$this->properties->fields = [
			'name',
			'firstname',
			'lastname',
			'email',
			'password',
			'permissions',
			'language',
			'status',
			'wheel_access',
			'imgext',
			'extradata',
		];
		$this->settings->resizeSizes = [
			'th' => [150,150,true,-1],
			'sm' => [300,300,false,-1]
		];
		$this->settings->mediumLimit = 0;
	}

	public function password_verify($password,$originalData){
		return (password_verify($password, $originalData['password']));
	}

    public function login(){
        if($_POST['email'] != '' && $_POST['password'] != ''){
            $user = $this->add_db_field('password')->get_list(null,1,["email" => $_POST['email']]);

            if(ion\helpers::get()->validate($user)->is_array()){
                $verify = $this->password_verify($_POST['password'],$user[0]);
                if($verify) {
                    $this->settings->response = [
                        'status' => 200,
                        'data' => $user[0]
                    ];
                } else {
                    $this->settings->response = [
                        'status' => 401,
                        'data' => 'Kullanıcı bilgilerinizi hatalı girdiniz'
                    ];
                }
            }else{
                $this->settings->response = [
                    'status' => 401,
                    'data' => 'Kullanıcı bilgilerinizi hatalı girdiniz'
                ];
            }
        }
        return $this->settings->response;
    }

	/**
	 * @param $sitemap
	 * @param $user
	 * @return string
	 */
	public function user_roles($sitemap,$user,$wrapperClass=''){
		$curUser = ion\front\auth::get()->get_user();
		$user['login'] = true;

        $mode = ion\ion::get()->get_current_app()->AUTH_MODE;
		if(count($sitemap) > 0){
			$text = '<ul class="list-unstyled indented">';
			foreach($sitemap->node AS $node){
				$test = ion\front\sitemap::get()->check_permissions($node,$user);
				$testSelf = ion\front\sitemap::get()->check_permissions($node);
				$checked = ($test == 200) ? ' checked="checked"' : '';
				$disabled = ($testSelf != 200 || ($mode != "BY_URL" && $node->permissions == 0) || ($mode != "BY_URL" && $node->permissions == "")) ? ' disabled="disabled"' : '';
				$disabledClass = ($testSelf != 200 || $node->permissions == 0 || $node->permissions == "") ? ' class="'.$wrapperClass.'"' : '';
				$children = $this->user_roles($node, $user);
                $perm = ($mode == 'BY_URL') ? $node->fullpath : $node->permissions;
				$text .= "	<li".$disabledClass."><div class=\"checkbox checkbox-success\"><input type=\"checkbox\" name=\"permissions[]\" value=\"".$perm."\"".$checked.$disabled." id=\"".$node->id."\" /> <label for=\"".$node->id."\">".$node->title."</label></div>".$children."</li>\r\n";
			}
			$text .= '</ul>';
		}

		return $text;
	}

	public function permissions_data(){
        $mode = ion\ion::get()->get_current_app()->AUTH_MODE;
        switch($mode){
            case 'BY_URL':
                return json_encode($_POST['permissions'],JSON_PRETTY_PRINT);
                break;
            default;
                return array_sum($_POST['permissions']);
        }
    }

	public function change_password($oldPassword=null,$override=true){
		$user = $this->add_db_field('password')->get_list(null,1,["email" => ion\front\auth::get()->get_user()['email']]);
		if($override === false && !$this->password_verify($oldPassword,$user[0])){
			$this->settings->response = ['status' => 0, 'data' => 'Şifrenizi Hatalı Girdiniz'];
			return false;
		}

		$helpers = ion\helpers::get();
		if($helpers->validate($_POST['n_password'])->is_null() === false || $helpers->validate($_POST['n_password_2'])->is_null() === false){
			if(strlen($_POST['n_password']) < 6){
				$this->settings->response = ['status' => 0, 'data' => 'Şifreniz çok kısa'];
				return false;
			}

			if($_POST['n_password'] == $_POST['n_password_2']){
				$new_pw = password_hash($_POST['n_password'],PASSWORD_BCRYPT);
				$this->set_field('password',$new_pw);
			}else{
				$this->settings->response = ['status' => 0, 'data' => 'Yeni şifreniz tekrarıyla uyuşmuyor'];
				return false;
			}
		}

		return $this;
	}

	public function helper_name_parser($name){
	    $nameArr = explode(' ',$name);
        $lastName = end($nameArr);
        $firstName = str_replace(' '.$lastName,null,$name);

        return ['firstname' => $firstName,'lastname' => $lastName];
    }

	protected function insert_callback(){
		$this->add_medium();
	}

	protected function update_callback(){
		$this->add_medium();
	}

	protected function delete_callback(){
		$this->remove_medium_by_id();
	}
}