<?php
/**
 * Class front
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Date: 18.03.2015
 */

namespace burakg\ion\dbBase;

use burakg\ion\dbBase\Connection\StrategyMongoDB;
use burakg\ion\ion;
use burakg\ion\singletonTrait;

class dbConn {
    use singletonTrait;
    protected $connections,$connectionData;

    protected function init(){
        $this->connections = [];
        $this->connectionData = [];
        $this->options->initiated = false;
    }

    /**
     * @param bool|string $currentApp
     * @return dbConn
     */
    public function initConns($currentApp=true){
        if($this->options->initiated === false){
            $conns = ion::get()->get_current_app($currentApp)->APP_SETTINGS->connections->{CF_DOMAIN};

            if(is_array($conns)){
                foreach($conns AS $connection){
                    $this->addConn(
                        CF_DOMAIN.'-'.$connection->name,
                        $connection->type,
                        $connection->host,
                        $connection->port,
                        $connection->user,
                        $connection->password,
                        $connection->database,
                        $connection->charset,
                        $connection->options
                    );
                }
            }elseif(!is_array($conns) || count($conns) == 0){
                $defCon = ion::get()->get_current_app($currentApp)->APP_SETTINGS->connections->default;
                foreach($defCon AS $connection){
                    $this->addConn(
                        CF_DOMAIN.'-'.$connection->name,
                        $connection->type,
                        $connection->host,
                        $connection->port,
                        $connection->user,
                        $connection->password,
                        $connection->database,
                        $connection->charset,
                        $connection->options
                    );
                }
            }

            $this->options->initiated = true;
        }

        return $this;
    }

    /**
     * @param $connName
     * @param $type
     * @param string $host (Could be the path to database file for SQLite (or even :memory: for in memory implementation))
     * @param int $port
     * @param null $user
     * @param null $password
     * @param null $databaseName
     * @param string $charset
     * @param string $uriOptions
     * @return $this
     */
    public function addConn($connName,$type,$host,$port=3306,$user=null,$password=null,$databaseName=null,$charset='UTF8',$uriOptions=null){
        switch($type){
            case 'mongo':
                $this->connectionData[$connName] = [
                    'status' => false,
                    'type' => $type,
                    'host' => $host,
                    'port' => $port,
                    'user' => $user,
                    'password' => $password,
                    'databaseName' => $databaseName,
                    'charset' => $charset,
                    'connect' => function() use ($connName,$type,$host,$user,$password,$databaseName,$charset,$port,$uriOptions){
                        $connection = new StrategyMongoDB($host, $port, $databaseName, $user, $password, $uriOptions);
                        $connectionAttempt = $connection->connect();

                        $this->connectionData[$connName]['status'] = $connection->status;

                        if($connection->status)
                            $this->connections[$connName] = $connectionAttempt->selectDatabase($databaseName);
                    }
                ];
                break;

            case 'postgre':
            case 'pgsql':
                $this->connectionData[$connName] = [
                    'status' => false,
                    'type' => $type,
                    'host' => $host,
                    'port' => $port,
                    'user' => $user,
                    'password' => $password,
                    'databaseName' => $databaseName,
                    'charset' => $charset,
                    'connect' => function() use ($connName,$type,$host,$user,$password,$databaseName,$charset,$port){
                        $db = new \PDO('pgsql:host='.$host.';dbname='.$databaseName.';port='.$port, $user, $password);

                        $sql_com = 'SET NAMES "'.$charset.'" COLLATE "utf8_general_ci",'
                            .'character_set_client = "'.$charset.'", '
                            .'character_set_results = "'.$charset.'", '
                            .'character_set_connection = "'.$charset.'", '
                            .'character_set_server = "'.$charset.'", '
                            .'character_set_database = "'.$charset.'" ';
                        $db->query($sql_com);

                        $this->connectionData[$connName]['status'] = true;
                        $this->connections[$connName] = $db;
                        return $db;
                    }
                ];
                break;

            case 'sqlite':
                $this->connectionData[$connName] = [
                    'status' => false,
                    'type' => $type,
                    'host' => $host,
                    'port' => $port,
                    'user' => $user,
                    'password' => $password,
                    'databaseName' => $databaseName,
                    'charset' => $charset,
                    'connect' => function() use ($connName,$type,$host,$user,$password,$databaseName,$charset,$port){
                        $db = new \PDO('sqlite:'.$host);

                        $this->connectionData[$connName]['status'] = true;
                        $this->connections[$connName] = $db;
                        return $db;
                    }
                ];
                break;

            case 'mysql':
            default:
                $this->connectionData[$connName] = [
                    'status' => false,
                    'type' => $type,
                    'host' => $host,
                    'port' => $port,
                    'user' => $user,
                    'password' => $password,
                    'databaseName' => $databaseName,
                    'charset' => $charset,
                    'connect' => function() use ($connName,$type,$host,$user,$password,$databaseName,$charset,$port){
                        $db = new \PDO('mysql:host='.$host.';dbname='.$databaseName.';port='.$port, $user, $password);

                        $sql_com = 'SET NAMES "'.$charset.'" COLLATE "utf8_general_ci",'
                            .'character_set_client = "'.$charset.'", '
                            .'character_set_results = "'.$charset.'", '
                            .'character_set_connection = "'.$charset.'", '
                            .'character_set_server = "'.$charset.'", '
                            .'character_set_database = "'.$charset.'" ';
                        $db->query($sql_com);

                        $this->connectionData[$connName]['status'] = true;
                        $this->connections[$connName] = $db;
                        return $db;
                    }
                ];
        }

        return $this;
    }

    /**
     * @param string $name
     * @return \PDO | bool
     */
    public function connect($name){
        $name = CF_DOMAIN.'-'.$name;
        if(array_key_exists($name,$this->connectionData) && $this->connectionData[$name]['status'] === false){
            call_user_func($this->connectionData[$name]['connect']);

            return $this->connections[$name];
        }elseif(array_key_exists($name,$this->connectionData) && $this->connectionData[$name]['status'] === true){
            return $this->connections[$name];
        }else{
            return false;
        }
    }

    /**
     * @param $name
     * @return array
     */
    public function getConnectionInfo($name){
        return $this->connectionData[$name];
    }

    /**
     * @param $name
     * @return \MongoDB\Client | bool
     */
    public function connectMongo($name){
        $name = CF_DOMAIN.'-'.$name;

        if(array_key_exists($name,$this->connectionData) && $this->connectionData[$name]['status'] === false){
            call_user_func($this->connectionData[$name]['connect']);

            return $this->connections[$name];
        }elseif(array_key_exists($name,$this->connectionData) && $this->connectionData[$name]['status'] === true){
            return $this->connections[$name];
        }else{
            return false;
        }
    }
}
