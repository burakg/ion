<?php
/**
 * ION Pagination
 * v1.0
 *
 * Author: Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 *
 * The ION Pagination component is used for handling pagination jobs by dbBase extensions.
 */

namespace burakg\ion\dbBase;

use burakg\ion\helpers\phraser;
use burakg\ion\helpers\template;

class pagination
{
    protected
        $options = [],
        $currentPage = 1,
        $nakedURL,
        $queryStringItems=[],
        $output;

    /**
     * pagination constructor.
     *
     * Enter either queryString or slug url mode
     * @param string $mode
     * @param string $queryStringParameterName
     */
    public function __construct($mode='queryString',$queryStringParameterName='page')
    {
        $this->options = [
            'totalPages' => 1,
            'totalRows' => 1,
            'rowsPerPage' => 1,
            'adjacents' => 3,
            'layoutShowNumbers' => true,
            'layoutShowPrevNext' => true,
            'layoutShowFirstLast' => true,
            'labelFirst' => phraser::get()->translate('FIRST'),
            'labelLast' => phraser::get()->translate('LAST'),
            'labelPrev' => phraser::get()->translate('PREV'),
            'labelNext' => phraser::get()->translate('NEXT'),
            'labelDots' => '...',
            'activeNodeClass' => 'active',
            'mode' => $mode,
            'queryStringParameterName' => $queryStringParameterName
        ];

        switch($this->options['mode']){
            case 'queryString':
                $this->nakedURL = explode('?',CF_URL)[0];
                foreach($_GET AS $param => $value){
                    $this->queryStringItems[$param] = $value;
                }
                $this->currentPage = (isset($_GET[$this->options['queryStringParameterName']])) ? $_GET[$this->options['queryStringParameterName']] : 1;
                break;

            case 'slug':
                // TODO Implement slug based initialization
                break;
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->options[$name];
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->options[$name] = $value;
    }

    /**
     * @param int $value
     * @return pagination
     */
    public function setTotalRows($value)
    {
        $this->options['totalRows'] = $value;

        return $this;
    }

    /**
     * @param int $value
     * @return pagination
     */
    public function setRowsPerPage($value)
    {
        $this->options['rowsPerPage'] = $value;

        return $this;
    }

    /**
     * @param int $value
     * @return pagination
     */
    public function setAdjacents($value)
    {
        $this->options['adjacents'] = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->currentPage;
    }

    /**
     * @return string
     */
    public function getQueryStringParameterName()
    {
        return $this->options['queryStringParameterName'];
    }

    /**
     * @return pagination
     */
    protected function calculateTotalPages()
    {
        $this->options['totalPages'] = ceil($this->options['totalRows'] / $this->options['rowsPerPage']);

        return $this;
    }

    /**
     * @return string|null
     */
    protected function elementFirst()
    {
        if($this->options['layoutShowFirstLast']){
            $template = template::get();
            $templateField = ($this->getCurrentPage() > 1) ? 'paginationItemActive' : 'paginationItemPassive';

            return $template->apply($templateField,[
                'label' => $this->options['labelFirst'],
                'url' => $this->buildUrl(1),
                'class' => ' pagination-first'
            ]);
        }else{
            return null;
        }
    }

    /**
     * @return string|null
     */
    protected function elementPrev()
    {
        if($this->options['layoutShowPrevNext']){
            $template = template::get();
            $templateField = ($this->getCurrentPage() > 1) ? 'paginationItemActive' : 'paginationItemPassive';

            return $template->apply($templateField,[
                'label' => $this->options['labelPrev'],
                'url' => $this->buildUrl($this->getCurrentPage()-1),
                'class' => ' pagination-prev'
            ]);
        }else{
            return null;
        }
    }

    /**
     * @return string|null
     */
    protected function elementNext()
    {
        if($this->options['layoutShowPrevNext']){
            $template = template::get();
            $templateField = ($this->getCurrentPage() < $this->options['totalPages']) ? 'paginationItemActive' : 'paginationItemPassive';

            return $template->apply($templateField,[
                'label' => $this->options['labelNext'],
                'url' => $this->buildUrl($this->getCurrentPage()+1),
                'class' => ' pagination-next'
            ]);
        }else{
            return null;
        }
    }

    /**
     * @return string|null
     */
    protected function elementLast()
    {
        if($this->options['layoutShowFirstLast']){
            $template = template::get();
            $templateField = ($this->getCurrentPage() < $this->options['totalPages']) ? 'paginationItemActive' : 'paginationItemPassive';

            return $template->apply($templateField,[
                'label' => $this->options['labelLast'],
                'url' => $this->buildUrl($this->options['totalPages']),
                'class' => ' pagination-last'
            ]);
        }else{
            return null;
        }
    }

    /**
     * @return string|null
     */
    protected function createNumbers()
    {
        $output = null;
        if($this->options['layoutShowNumbers']){
            $template = template::get();

            if($this->options['adjacents'] > $this->options['totalPages']){
                for($i=1;$i<=$this->options['totalPages'];$i++){
                    $templateField = ($this->getCurrentPage() != $i) ? 'paginationItemActive' : 'paginationItemPassive';
                    $class = ($this->getCurrentPage() != $i) ? ' pagination-item' : ' pagination-item '.$this->options['activeNodeClass'];

                    $output .= $template->apply($templateField,[
                        'label' => $i,
                        'url' => $this->buildUrl($i),
                        'class' => $class
                    ]);
                }
            }else{
                $itemsBefore = ceil($this->options['adjacents']/2);
                $itemsAfter = floor($this->options['adjacents']/2);

                $startsAt = max(1,($this->getCurrentPage()-$itemsBefore+1));
                $endsAt = min($this->options['adjacents'] + $startsAt-1,$this->options['totalPages']);

                for($i=$startsAt;$i<=$endsAt;$i++){
                    $templateField = ($this->getCurrentPage() != $i) ? 'paginationItemActive' : 'paginationItemCurrent';
                    $class = ($this->getCurrentPage() != $i) ? ' pagination-item' : ' pagination-item '.$this->options['activeNodeClass'];

                    $output .= $template->apply($templateField,[
                        'label' => $i,
                        'url' => $this->buildUrl($i),
                        'class' => $class
                    ]);
                }
            }
        }

        return $output;
    }

    /**
     * @param int $pageNumber
     * @return string
     */
    protected function buildUrl($pageNumber)
    {
        if($pageNumber > 1){
            switch($this->options['mode']){
                case 'queryString':
                    $queryStringItems = array_diff_key(
                        $this->queryStringItems,
                        [
                            $this->options['queryStringParameterName'] => $this->queryStringItems[$this->options['queryStringParameterName']]
                        ]
                    );
                    $queryStringItems[$this->options['queryStringParameterName']] = $pageNumber;
                    return $this->nakedURL.'?'.http_build_query($queryStringItems);
                    break;
                case 'slug': // TODO Implement slug based URL build procedure
                default:
                    return '';
            }
        }else{
            return $this->nakedURL;
        }
    }

    /**
     * @return string
     */
    public function render()
    {
        $output = null;
        $this->calculateTotalPages();
        if($this->options['totalPages'] <= 1)
            return null;

        $output .= $this->elementFirst();
        $output .= $this->elementPrev();
        $output .= $this->createNumbers();
        $output .= $this->elementNext();
        $output .= $this->elementLast();

        return $output;
    }
}