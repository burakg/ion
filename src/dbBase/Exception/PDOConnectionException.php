<?php
namespace burakg\ion\dbBase\Exception;

class PDOConnectionException
{
    /**
     * @param $serverName
     * @param $port
     * @return PDOConnectionException
     */
    public static function cannotConnectToServer($serverName, $port)
    {
        return new static(sprintf('Could not connect to the server "%s" at "%s" port, please check your connection settings', $serverName, $port));
    }
}
