<?php
namespace burakg\ion\dbBase\Exception;

class MongoConnectionException extends \Exception
{
    /**
     * @param string $serverName
     * @param string $port
     * @param string $message
     * @return MongoConnectionException
     */
    public static function cannotConnectToServer($serverName, $port, $code, $message)
    {
        return new static(sprintf('Could not connect to the server "%s" at "%s" port with error code: (%s)', $serverName, $port, $code, $message));
    }
}
