<?php
/**
 * ION Suite dbBase Response Object
 * The object is a simple container with five property
 *
 * The class transforms its properties into a json encoded string
 * when it's echoed
 */

namespace burakg\ion\dbBase;


class PDOResponse
{

    /**
     * Carries the boolean response status
     *
     * @var boolean
     */
    public $status;

    /**
     * Carries the response code
     *
     * @var integer
     */
    public $code;

    /**
     * Carries the response message
     *
     * @var string
     */
    public $message;

    /**
     * Carries the output data where applicable
     *
     * @var array
     */
    public $data=[];

    /**
     * @var int
     */
    public $currentPage=0;

    /**
     * PDOResponse constructor.
     * @param boolean $status
     * @param int $code
     * @param string $message
     * @param array $data
     * @param int $currentPage
     */
    public function __construct($status,$code,$message,$data=null,$currentPage=0)
    {
        $this->status = $status;
        $this->code = $code;
        $this->message = $message;
        $this->data = $data;
        $this->currentPage = $currentPage;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value){}

    /**
     * @return string
     */
    public function __toString()
    {
        return json_encode([
            'status' => $this->status,
            'code' => $this->code,
            'message' => $this->message,
            'data' => $this->data,
        ]);
    }
}