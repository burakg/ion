<?php
/**
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Date: 14.11.2015
 */

namespace burakg\ion\dbBase;
use burakg\ion AS ion;
use burakg\logger;
use stefangabos\zebrapagination as zebra;

trait mongoBaseTrait {
	public function __construct($connectionName='mongo'){
		$this->settings = new ion\generic\option;
		$this->properties = new ion\generic\option;

		$this->settings->connName = $connectionName;
		$this->properties->dbConn = ion\ion::get()->db()->initConns()->connect($connectionName);
		$this->properties->fields = [];
		$this->properties->uniqueField = '_id';

		$this->init();

		$this->properties->collection = $this->properties->dbConn->selectCollection($this->properties->tableName);
	}

	public function __get($key){
		return $this->settings->$key;
	}

	public function __set($key,$val){
		$this->settings->$key = $val;
	}

	/**
	 * @return $this
	 */
	public function save(){
		$fields_dup = $this->properties->fields;
		$fields_dup_set = $this->properties->fields;
		array_walk($fields_dup, function(&$value, $index){$value = ":".$value;});
		array_walk($fields_dup_set, function(&$value, $index){$value = "`".$value."`";});

		if($this->settings->id == 0){
			$this->properties->set = "(".implode(', ',$fields_dup_set).") VALUES"."(".implode(', ',$fields_dup).")";

			$this->insert();
			$this->insert_callback();
		}elseif($this->settings->id > 0){
			$set = [];
			for($i=0;$i<count($fields_dup_set);$i++)
				$set[] = $fields_dup_set[$i]." = ".$fields_dup[$i];

			$this->properties->set = implode(', ', $set);

			$this->update();
			$this->update_callback();
		}
		return $this;
	}

	/**
	 * @param $field
	 * @return $this
	 */
	final public function add_db_field($field){
		$fields = $this->properties->fields;
		$fields[] = $field;
		$this->properties->fields = $fields;

		return $this;
	}

	/**
	 * @param $field
	 * @return $this
	 */
	final public function remove_db_field($field){
		if(($key = array_search($field, $this->properties->fields)) !== false){
			$this->properties->fields = array_values(array_diff($this->properties->fields,[$field]));
		}

		return $this;
	}

	private function update(){
		$this->properties->boundData = [];

		foreach($this->properties->fields AS $field){
			$this->properties->boundData[$field] = (isset($this->settings->presetValues[$field])) ? $this->settings->presetValues[$field] : $_POST[$field];
		}

		try{
			$ex = $this->properties->collection->update([$this->properties->uniqueField => $this->settings->id],$this->properties->boundData);

			if($ex){
				$this->settings->response = ion\helpers::get()->data(
					[
						'data' => ion\helpers\phraser::get()->translate('UPDATE_SUCCESSFUL'),
						'status' => 1
					]
				)->ajax_response();
				$this->settings->affectedRows = $collection->rowCount();
				logger\ionLogger::log(get_class($this), 1, 'updateSuccess', $this->properties->tableName,$this->settings->id);
			}else{
				logger\ionLogger::log(get_class($this), 1, 'updateFail - '.implode(',',$collection->errorInfo()), $this->properties->tableName,$this->settings->id);
				$this->settings->affectedRows = 0;
				$this->settings->response = ion\helpers::get()->data(
					[
						'data' => ion\helpers\phraser::get()->translate('UPDATE_UNSUCCESSFUL'),
						'status' => 0
					]
				)->ajax_response();
			}
		}catch(\Exception $e){
			logger\ionLogger::log(get_class($this), 1, 'updateFail - '.$collection->errorCode(), $this->properties->tableName,$this->settings->id);
			$this->settings->response = ion\helpers::get()->data(
				[
					'data' => ion\helpers\phraser::get()->translate('UPDATE_UNSUCCESSFUL'),
					'status' => 0
				]
			)->ajax_response();
		}

		if($this->settings->debug === true) {
			$this->settings->errorInfo = $collection->errorInfo();
		}

		return $this->settings->response;
	}

	private function insert(){
		$sql = $this->properties->dbConn->prepare("INSERT IGNORE INTO ".$this->properties->tableName." ".$this->properties->set);

		foreach($this->properties->fields AS $field){
			if(isset($this->settings->presetValues[$field]))
				$sql->bindValue(':'.$field,$this->settings->presetValues[$field],$this->bind_param_null($this->settings->presetValues[$field]));
			elseif(isset($_POST[$field]))
				$sql->bindValue(':'.$field,$_POST[$field],$this->bind_param_null($_POST[$field]));
			else
				$sql->bindValue(':'.$field,"",$this->bind_param_null(null));
		}

		try{
			$ex = $sql->execute();

			if($ex){
				$this->settings->id = $this->properties->dbConn->lastInsertId();
				$this->settings->affectedRows = $sql->rowCount();
				$this->settings->response = ion\helpers::get()->data(
					[
						'data' => ion\helpers\phraser::get()->translate('INSERT_SUCCESSFUL'),
						'status' => 1
					]
				)->ajax_response();
				logger\ionLogger::log(get_class($this), 1, 'createSuccess', $this->properties->tableName, $this->settings->id);
			}else{
				logger\ionLogger::log(get_class($this), $sql->errorCode(), 'createFail - '.implode(',',$sql->errorInfo()), $this->properties->tableName, $this->settings->id);
				$this->settings->affectedRows = 0;
				$this->settings->response = ion\helpers::get()->data(
					[
						'data' => ion\helpers\phraser::get()->translate('INSERT_UNSUCCESSFUL'),
						'status' => 0
					]
				)->ajax_response();
			}
		}catch(\PDOException $e){
			logger\ionLogger::log(get_class($this), $e->getCode(), 'createFail - '.$e->getMessage(), $this->properties->tableName, $this->settings->id);
			$this->settings->response = ion\helpers::get()->data(
				[
					'data' => ion\helpers\phraser::get()->translate('INSERT_UNSUCCESSFUL'),
					'status' => 0
				]
			)->ajax_response();
		}

		if($this->settings->debug === true)
			$this->settings->errorInfo = $sql->errorInfo();

		return $this->settings->response;
	}

	/**
	 * @param null $prefix
	 * @return mixed
	 */
	public function delete($prefix=null){
		$q = ($this->settings->restricted === true) ? "DELETE FROM ".$this->properties->tableName." WHERE ".$this->properties->uniqueField." = ? AND {$this->properties->user_field} = ?" : "DELETE FROM ".$this->properties->tableName." WHERE ".$this->properties->uniqueField." = ?";
		$sql = $this->properties->dbConn->prepare($q);

		try{
			if($this->settings->restricted === true)
				$sql->execute([$this->settings->id,$_SESSION['uid']]);
			else
				$sql->execute([$this->settings->id]);
			$this->delete_callback();

			$this->properties->affectedRows = $sql->rowCount();

			$this->settings->response = ion\helpers::get()->data(
				[
					'data' => 'ION.ajaxCallbacks.removeRow("'.$prefix.$this->settings->id.'");',
					'status' => 1
				]
			)->ajax_response();

			logger\ionLogger::log(get_class($this), 1, 'deleteSuccess', $this->properties->tableName, $this->settings->id);
		}catch(\Exception $e){
			logger\ionLogger::log(get_class($this), $e->getCode(), $e->getMessage(), $this->properties->tableName, $this->settings->id);
			$this->settings->response = ion\helpers::get()->data(
				[
					'data' => 'void(0);',
					'status' => 0
				]
			)->ajax_response();
		}
		return $this->settings->response;
	}

	/**
	 * @param string $field
	 * @param int $value
	 * @return mixed
	 */
	public function set_field($field='status',$value=0){
		$sql = $this->properties->dbConn->prepare("UPDATE ".$this->properties->tableName." SET {$field} = :value WHERE ".$this->properties->uniqueField." = :ID");
		$sql->bindValue(":value", $value);
		$sql->bindValue(":ID", $this->settings->id);

		try{
			$ex = $sql->execute();
			if($ex){
				logger\ionLogger::log(get_class($this), 1, $field.' state set to '.$value.' by '.$_SESSION['uid'],$this->properties->tableName,$this->settings->id);
				$this->settings->response = ion\helpers::get()->data(
					[
						'data' => ion\helpers\phraser::get()->translate('SET_STATE_SUCCESSFUL'),
						'status' => 1
					]
				)->ajax_response();
			}else{
				logger\ionLogger::log(get_class($this), 0, 'setStateFail - '.$sql->errorCode(), $this->properties->tableName, $this->settings->id);
				$this->settings->response = ion\helpers::get()->data(
					[
						'data' => ion\helpers\phraser::get()->translate('SET_STATE_UNSUCCESSFUL'),
						'status' => 0
					]
				)->ajax_response();
			}
		}catch(\Exception $e){
			logger\ionLogger::log(get_class($this), $e->getCode(), $e->getMessage(), $this->properties->tableName, $this->settings->id);
			$this->settings->response = ion\helpers::get()->data(
				[
					'data' => ion\helpers\phraser::get()->translate('SET_STATE_UNSUCCESSFUL'),
					'status' => 0
				]
			)->ajax_response();
		}
		return $this->settings->response;
	}

	/**
	 * @param string $field
	 * @return array|string
	 */
	public function switch_field($field='status'){
		$sql = $this->properties->dbConn->prepare("UPDATE ".$this->properties->tableName." SET {$field} = {$field} XOR 1 WHERE ".$this->properties->uniqueField." = :ID");
		$sql->bindValue(":ID", $this->settings->id);

		try{
			$ex = $sql->execute();
			if($ex){
				$state = json_decode($this->get_state('status'));

				logger\ionLogger::log(get_class($this), 1, $field.' state set to '.$state->status.' by '.$_SESSION['uid'],$this->properties->tableName,$this->settings->id);
				$this->settings->response =  'ION.callBacks.setState($("#row_'.$this->settings->id.' i.fa-status").parent(),"green",'.(int)$state->status.')';
			}else{
				logger\ionLogger::log(get_class($this), 0, 'setStateFail - '.$sql->errorCode(), $this->properties->tableName, $this->settings->id);
				$this->settings->response = ion\helpers::get()->data(
					[
						'data' => ion\helpers\phraser::get()->translate('SET_STATE_UNSUCCESSFUL'),
						'status' => 0
					]
				)->ajax_response();
			}
		}catch(\Exception $e){
			logger\ionLogger::log(get_class($this), $e->getCode(), $e->getMessage(), $this->properties->tableName, $this->settings->id);
			$this->settings->response = ion\helpers::get()->data(
				[
					'data' => ion\helpers\phraser::get()->translate('SET_STATE_UNSUCCESSFUL'),
					'status' => 0
				]
			)->ajax_response();
		}
		return $this->settings->response;
	}

	/**
	 * @param string $field
	 * @return mixed
	 */
	public function get_field($field='status'){
		$sql = $this->properties->dbConn->prepare("SELECT {$field} FROM ".$this->properties->tableName." WHERE ".$this->properties->uniqueField." = :ID");
		$sql->bindValue(':ID', $this->settings->id);

		try{
			$sql->execute();
			$rs = $sql->fetch();
			$this->settings->response = ion\helpers::get()->data(
				[
					'data' => ion\helpers\phraser::get()->translate('GET_STATE_SUCCESSFUL'),
					'status' => $rs[$field]
				]
			)->ajax_response();
		}catch(\Exception $e){
			logger\ionLogger::log(get_class($this), $e->getCode(), $e->getMessage(), $this->properties->tableName, $this->settings->id);
			$this->settings->response = ion\helpers::get()->data(
				[
					'data' => ion\helpers\phraser::get()->translate('GET_STATE_UNSUCCESSFUL'),
					'status' => 0
				]
			)->ajax_response();
		}
		return $this->settings->response;
	}

	/**
	 * @return array
	 */
	public function get(){
		$data = [];

		$restrict_user = ($this->settings->restricted === true) ? " AND {$this->properties->user_field} = ".$_SESSION['uid'] : "";

		$query = "SELECT * FROM `".$this->properties->tableName."` WHERE ".$this->properties->uniqueField." = ".$this->settings->id.$restrict_user;
		$sql = $this->properties->dbConn->prepare($query);
		$sql->execute();

		$rs = $sql->fetch();
		foreach($this->properties->fields AS $field) $data[$field] = $rs[$field];
		$data['id'] = $rs['id'];

		$this->settings->details = $data;

		return $data;
	}

	/**
	 * @param null $suffix
	 * @param int $paginate
	 * @param array $query
	 * @param null $order
	 * @param null $prefix
	 * @return array|bool
	 */
	public function get_list($suffix=null,$paginate=0,$query=[],$order=null,$prefix=null){
		if($this->settings->restricted === true) $query[$this->properties->user_field] = $_SESSION['uid'];

		$build = [];
		foreach($query AS $key => $value){
			if($value != ''){
				if(is_array($value) && in_array($value[1],["LIKE","IN","<",">","<>"])){
					$build[] = $key.' '.$value[1].' :'.$key;
				}else{
					$build[] = $key.' = :'.$key;
				}
			}
		}

		$builtQuery = implode(' AND ', $build);
		if(strlen($builtQuery) > 0) $builtQuery = ' AND '.$builtQuery;

		$q = "SELECT SQL_CALC_FOUND_ROWS * FROM {$this->properties->tableName} {$prefix} WHERE 1".$suffix.$builtQuery.$order;

		if($paginate > 0){
			$start = ($_GET['page'] > 1) ? $paginate*($_GET['page']-1) : 0;
			$q .= " LIMIT {$start},".$paginate;

			$this->settings->pagination = new zebra\zebrapagination;
		}

		try{
			$sql = $this->properties->dbConn->prepare($q);
			foreach($query AS $key => $value){
				if($value != '' && is_array($value)) $sql->bindValue(":".$key, $value[0]);
				elseif($value != '' && !is_array($value)) $sql->bindValue(":".$key, $value);
			}

			$ex = $sql->execute();

			if($this->settings->debug === true && $ex === false)
				print_r($sql->errorInfo());

			$found = $this->properties->dbConn->query('SELECT FOUND_ROWS()');
			$this->settings->maxRows = $found->fetch(\PDO::FETCH_COLUMN);

			if($paginate > 0){
				if($this->settings->showNumbers === true) $this->settings->pagination->set_opt('showNumbers',true);
				$this->settings->pagination->records($this->settings->maxRows);
				$this->settings->pagination->records_per_page($paginate);
				$this->settings->pagination->labels(ion\helpers\phraser::get()->translate('PREVIOUS'),ion\helpers\phraser::get()->translate('NEXT'));
				$this->settings->pagination->selectable_pages(5);
			}

		}catch(\Exception $e){
			logger\ionLogger::log(get_class($this), $e->getCode(), print_r($e->getMessage()),$this->settings->id,$this->db);
			return false;
		}

		$i=0;
		$list=[];

		if((int)$this->settings->maxRows > 0){
			while($rs = $sql->fetch()){
				foreach($this->properties->fields AS $field)
					$list[$i][$field] = $rs[$field];
				$list[$i]['id'] = $rs['id'];
				$i++;
			}
		}else{
			return ion\helpers\phraser::get()->translate('NO_RECORDS_FOUND');
		}

		return $list;
	}

	/**
	 * @return \stefangabos\zebrapagination\zebrapagination;
	 */
	public function pagination(){
		return $this->settings->pagination;
	}

	protected function insert_callback(){}
	protected function update_callback(){}
	protected function delete_callback(){}
}