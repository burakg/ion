<?php
/**
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 20.03.2015
 */

namespace burakg\ion\dbBase;
use burakg\ion AS ion;
use burakg\logger\logger;

/**
 * Class dbBaseTrait
 * @package burakg\ion\dbBase
 */
trait dbBaseTrait {
    public function __construct($connectionName='front'){
        $this->settings = new ion\generic\option;
        $this->properties = new ion\generic\option;

        $this->settings->connName = $connectionName;
        $this->properties->dbConn = ion\ion::get()->db()->initConns()->connect($connectionName);
        $this->properties->fields = [];
        $this->properties->uniqueField = 'id';
        $this->properties->user_field = 'userid';
        $this->settings->logDbTrace = true;

        $this->init();
    }

    public function __get($key){
        return $this->settings->$key;
    }

    public function __set($key,$val){
        $this->settings->$key = $val;
    }

    /**
     * @param bool $updateOnDuplicate
     * @return $this
     */
    public function save($updateOnDuplicate=false){
        $fields_dup = $this->properties->fields;
        $fields_dup_set = $this->properties->fields;
        array_walk($fields_dup, function(&$value, $index){$value = ":".$value;});
        array_walk($fields_dup_set, function(&$value, $index){$value = "`".$value."`";});

        if($this->settings->id == 0){
            $this->properties->set = "(".implode(', ',$fields_dup_set).") VALUES"."(".implode(', ',$fields_dup).")";

            $this->insert($updateOnDuplicate);
            $this->insert_callback();
        }elseif((!is_numeric($this->settings->id) && $this->settings->id != '') || $this->settings->id > 0){
            $set = [];
            for($i=0;$i<count($fields_dup_set);$i++)
                $set[] = $fields_dup_set[$i]." = ".$fields_dup[$i];

            $this->properties->set = implode(', ', $set);

            $this->update();
            $this->update_callback();
        }
        return $this;
    }

    /**
     * @param $field
     * @return $this
     */
    final public function add_db_field($field){
        if(!in_array($field,$this->properties->fields)){
            $fields = $this->properties->fields;
            $fields[] = $field;
            $this->properties->fields = $fields;
        }

        return $this;
    }

    /**
     * @param $field
     * @return $this
     */
    final public function remove_db_field($field){
        if(($key = array_search($field, $this->properties->fields)) !== false){
            $this->properties->fields = array_values(array_diff($this->properties->fields,[$field]));
        }

        return $this;
    }

    /**
     * @param $val
     * @return bool
     */
    final protected function bind_param_null($val){
        if(is_int($val))
            $param = \PDO::PARAM_INT;
        elseif(is_bool($val))
            $param = \PDO::PARAM_BOOL;
        elseif(is_null($val))
            $param = \PDO::PARAM_NULL;
        elseif(is_string($val))
            $param = \PDO::PARAM_STR;
        else
            $param = FALSE;

        return $param;
    }

    private function update(){
        $this->sqlQuery = $this->doQuery()->prepare("UPDATE ".$this->properties->tableName." SET ".$this->properties->set." WHERE ".$this->properties->uniqueField." = :uniqueId");
        $this->sqlQuery->bindValue(':uniqueId',$this->settings->id);

        foreach($this->properties->fields AS $field){
            if(isset($this->settings->presetValues[$field]))
                $this->sqlQuery->bindValue(':'.$field,$this->settings->presetValues[$field],$this->bind_param_null($this->settings->presetValues[$field]));
            elseif(isset($_POST[$field]))
                $this->sqlQuery->bindValue(':'.$field,$_POST[$field],$this->bind_param_null($_POST[$field]));
            else
                $this->sqlQuery->bindValue(':'.$field,"",$this->bind_param_null(null));
        }

        try{
            $ex = $this->sqlQuery->execute();

            if($ex){
                if($this->settings->logDbTrace === true) logger::log(get_class($this), 1, 'updateSuccess', $this->properties->tableName,$this->settings->id);
                $this->settings->affectedRows = $this->sqlQuery->rowCount();

                $this->settings->response = new PDOResponse(true, 1, ion\helpers\phraser::get()->translate('UPDATE_SUCCESSFUL'));
            }else{
                if($this->settings->logDbTrace === true) logger::log(get_class($this), 1, 'updateFail - '.implode(',',$this->sqlQuery->errorInfo()), $this->properties->tableName,$this->settings->id);
                $this->settings->affectedRows = 0;

                $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('UPDATE_UNSUCCESSFUL'));
            }
        }catch(\Exception $e){
            if($this->settings->logDbTrace === true) logger::log(get_class($this), 1, 'updateFail - '.$this->sqlQuery->errorCode(), $this->properties->tableName,$this->settings->id);
            $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('UPDATE_UNSUCCESSFUL').' - PDO Error', [
                'message' => $e->getMessage(),
                'code' => $e->getCode()
            ]);
        }

        if($this->settings->debug === true) {
            $this->settings->errorInfo = $this->sqlQuery->errorInfo();
        }

        return $this->settings->response;
    }

    private function insert($updateOnDuplicate=false){
        if($updateOnDuplicate === false) {
            $ignore = (ion\ion::get()->db()->getConnectionInfo($this->settings->connName)['type']) ? " IGNORE" : null;
            $this->sqlQuery = $this->doQuery()->prepare("INSERT {$ignore} INTO " . $this->properties->tableName . " " . $this->properties->set);
        }else{
            $suffix = (isset($this->settings->presetValues[$updateOnDuplicate])) ? "'".$this->settings->presetValues[$updateOnDuplicate]."'" : "'".$_POST[$updateOnDuplicate]."'";
            $this->sqlQuery = $this->doQuery()->prepare("INSERT INTO ".$this->properties->tableName." ".$this->properties->set." ON DUPLICATE KEY UPDATE ".$updateOnDuplicate." = ".$suffix);
        }

        foreach($this->properties->fields AS $field){
            if(isset($this->settings->presetValues[$field]))
                $this->sqlQuery->bindValue(':'.$field,$this->settings->presetValues[$field],$this->bind_param_null($this->settings->presetValues[$field]));
            elseif(isset($_POST[$field]))
                $this->sqlQuery->bindValue(':'.$field,$_POST[$field],$this->bind_param_null($_POST[$field]));
            else
                $this->sqlQuery->bindValue(':'.$field,"",$this->bind_param_null(null));
        }

        try{
            $ex = $this->sqlQuery->execute();

            if($ex){
                $this->settings->id = $this->doQuery()->lastInsertId();
                if($this->settings->logDbTrace === true) logger::log(get_class($this), 1, 'createSuccess', $this->properties->tableName, $this->settings->id);

                $this->settings->affectedRows = $this->sqlQuery->rowCount();

                $this->settings->response = new PDOResponse(true, 1, ion\helpers\phraser::get()->translate('INSERT_SUCCESSFUL'));
            }else{
                if($this->settings->logDbTrace === true) logger::log(get_class($this), $this->sqlQuery->errorCode(), 'createFail - '.implode(',',$this->sqlQuery->errorInfo()), $this->properties->tableName, $this->settings->id);
                $this->settings->affectedRows = 0;

                $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('INSERT_UNSUCCESSFUL').' - PDO Error');
            }
        }catch(\PDOException $e){
            if($this->settings->logDbTrace === true) logger::log(get_class($this), $e->getCode(), 'createFail - '.$e->getMessage(), $this->properties->tableName, $this->settings->id);

            $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('INSERT_UNSUCCESSFUL').' - PDO Error', [
                'message' => $e->getMessage(),
                'code' => $e->getCode()
            ]);
        }

        if($this->settings->debug === true)
            $this->settings->errorInfo = $this->sqlQuery->errorInfo();

        return $this->settings->response;
    }

    /**
     * @return PDOResponse
     */
    public function delete(){
        $q = ($this->settings->restricted === true) ? "DELETE FROM ".$this->properties->tableName." WHERE ".$this->properties->uniqueField." = ? AND {$this->properties->user_field} = ?" : "DELETE FROM ".$this->properties->tableName." WHERE ".$this->properties->uniqueField." = ?";
        $this->sqlQuery = $this->doQuery()->prepare($q);

        try{
            if($this->settings->restricted === true){
                $user = ion\front\auth::get()->get_user();
                $this->sqlQuery->execute([$this->settings->id,$user['id']]);
            }else
                $this->sqlQuery->execute([$this->settings->id]);
            $this->delete_callback();

            $this->settings->affectedRows = $this->sqlQuery->rowCount();

            if($this->settings->logDbTrace === true) logger::log(get_class($this), 1, 'deleteSuccess', $this->properties->tableName, $this->settings->id);
            $this->settings->response = new PDOResponse(true, 1, ion\helpers\phraser::get()->translate('REMOVE_SUCCESSFUL'));
        }catch(\Exception $e){
            if($this->settings->logDbTrace === true) logger::log(get_class($this), $e->getCode(), $e->getMessage(), $this->properties->tableName, $this->settings->id);
            $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('REMOVE_UNSUCCESSFUL'));
        }
        return $this->settings->response;
    }

    /**
     * @return PDOResponse
     */
    public function soft_delete(){
        $q = $this->set_field('deleted',1);
        $this->delete_callback();

        if($this->settings->affectedRows == 1){
            $this->settings->response = new PDOResponse(true, 1, ion\helpers\phraser::get()->translate('REMOVE_SUCCESSFUL'));
        }else{
            $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('REMOVE_UNSUCCESSFUL'));
        }
        return $this->settings->response;
    }

    /**
     * @param string $field
     * @param int $value
     * @return PDOResponse
     */
    public function set_field($field='status',$value=0){
        $sql = $this->doQuery()->prepare("UPDATE ".$this->properties->tableName." SET {$field} = :value WHERE ".$this->properties->uniqueField." = :ID");
        $sql->bindValue(":value", $value);
        $sql->bindValue(":ID", $this->settings->id);

        try{
            $ex = $sql->execute();
            if($ex){
                $this->settings->affectedRows = $sql->rowCount();

                if($this->settings->logDbTrace === true) logger::log(get_class($this), 1, $field.' state set to '.$value.' by '.$_SESSION['uid'],$this->properties->tableName,$this->settings->id);
                $this->settings->response = new PDOResponse(true, 1, ion\helpers\phraser::get()->translate('SET_STATE_SUCCESSFUL'));
            }else{
                if($this->settings->logDbTrace === true) logger::log(get_class($this), 0, 'setStateFail - '.$sql->errorCode(), $this->properties->tableName, $this->settings->id);
                $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('SET_STATE_UNSUCCESSFUL'));
            }
        }catch(\Exception $e){
            if($this->settings->logDbTrace === true) logger::log(get_class($this), $e->getCode(), $e->getMessage(), $this->properties->tableName, $this->settings->id);
            $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('SET_STATE_UNSUCCESSFUL'));
        }

        return $this->settings->response;
    }

    /**
     * @return \PDO
     */
    public function doQuery(){
        return $this->properties->dbConn;
    }

    /**
     * @param string $field
     * @return PDOResponse
     */
    public function switch_field($field='status'){
        $sql = $this->doQuery()->prepare("UPDATE ".$this->properties->tableName." SET {$field} = {$field} XOR 1 WHERE ".$this->properties->uniqueField." = :ID");
        $sql->bindValue(":ID", $this->settings->id);

        try{
            $ex = $sql->execute();
            if($ex){
                $state = json_decode($this->get_field('status'));

                if($this->settings->logDbTrace === true) logger::log(get_class($this), 1, $field.' state set to '.$state->status.' by '.$_SESSION['uid'],$this->properties->tableName,$this->settings->id);
                $this->settings->response = new PDOResponse(true, 1, ion\helpers\phraser::get()->translate('SET_STATE_UNSUCCESSFUL'), [
                    'callback' => 'ION.callBacks.setState($("#row_'.$this->settings->id.' i.fa-status").parent(),"green",'.(int)$state->status.')',
                    'status' => (int)$state->status
                ]);
            }else{
                if($this->settings->logDbTrace === true) logger::log(get_class($this), 0, 'setStateFail - '.$sql->errorCode(), $this->properties->tableName, $this->settings->id);
                $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('SET_STATE_UNSUCCESSFUL'));
            }
        }catch(\Exception $e){
            if($this->settings->logDbTrace === true) logger::log(get_class($this), $e->getCode(), $e->getMessage(), $this->properties->tableName, $this->settings->id);
            $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('SET_STATE_UNSUCCESSFUL'));
        }

        return $this->settings->response;
    }

    /**
     * @param string $field
     * @return PDOResponse
     */
    public function get_field($field='status'){
        $sql = $this->doQuery()->prepare("SELECT {$field} FROM ".$this->properties->tableName." WHERE ".$this->properties->uniqueField." = :ID");
        $sql->bindValue(':ID', $this->settings->id);

        try{
            $sql->execute();
            $rs = $sql->fetch();

            $this->settings->response = new PDOResponse(true, 1, ion\helpers\phraser::get()->translate('GET_STATE_SUCCESSFUL'), $rs[$field]);
        }catch(\Exception $e){
            if($this->settings->logDbTrace === true) logger::log(get_class($this), $e->getCode(), $e->getMessage(), $this->properties->tableName, $this->settings->id);

            $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('GET_STATE_UNSUCCESSFUL'));
        }

        return $this->settings->response;
    }

    /**
     * @param null $id
     * @return PDOResponse
     */
    public function get($id=null){
        if(!is_null($id))
            $this->settings->id = $id;

        $data = [];

        $restrict_user = ($this->settings->restricted === true) ? " AND {$this->properties->user_field} = ".$_SESSION['uid'] : "";
        if($this->settings->id){
            $query = "SELECT * FROM `".$this->properties->tableName."` WHERE ".$this->properties->uniqueField." = :uniqueId".$restrict_user;
            $sql = $this->doQuery()->prepare($query);
            $sql->bindParam(':uniqueId',$this->settings->id);
            $sql->execute();

            $rs = $sql->fetch();

            if($rs['id'] == $this->settings->id){
                foreach($this->properties->fields AS $field) $data[$field] = $rs[$field];
                $data[$this->properties->uniqueField] = $rs[$this->properties->uniqueField];
                $this->settings->details = $data;

                $this->settings->response = new PDOResponse(true, 1, ion\helpers\phraser::get()->translate('N_RECORDS_FOUND',false,[1]), $data);
            }else{
                $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('NO_RECORDS_FOUND'), []);
            }
        }

        return $this->settings->response;
    }

    /**
     * @param null|string $suffix
     * @param int $paginate
     * @param array $query
     * @param null|string $order
     * @param null|string $prefix
     * @param null|string $fieldSelector
     * @return PDOResponse
     */
    public function get_list($suffix=null,$paginate=0,$query=[],$order=null,$prefix=null,$fieldSelector=null){
        if($this->settings->restricted === true) $query[$this->properties->user_field] = $_SESSION['uid'];

        $build = [];
        foreach($query AS $key => $value){
            if($value != ''){
                if(is_array($value) && in_array($value[1],["LIKE","IN","<",">","<>"])){
                    $build[] = $key.' '.$value[1].' :'.$key;
                }else{
                    $build[] = $key.' = :'.$key;
                }
            }
        }

        $builtQuery = implode(' AND ', $build);
        if(strlen($builtQuery) > 0) $builtQuery = ' AND '.$builtQuery;

        $fieldSelector = ($fieldSelector === null) ? $this->properties->tableName.'.*' : $fieldSelector;

        $connType = ion\ion::get()->db()->getConnectionInfo(CF_DOMAIN.'-'.$this->settings->connName)['type'];
        $counter = ($connType === 'mysql') ? ' SQL_CALC_FOUND_ROWS' : '';

        $q = "SELECT {$counter} {$fieldSelector} FROM {$this->properties->tableName} {$prefix} WHERE 1".$suffix.$builtQuery.$order;

        if($paginate > 0){
            $start = ($_GET['page'] > 1) ? $paginate*($_GET['page']-1) : 0;
            $q .= " LIMIT {$start},".$paginate;

            $this->settings->pagination = new pagination;
        }

        try{
            $this->sqlQuery = $this->doQuery()->prepare($q);
            foreach($query AS $key => $value){
                if($value != '' && is_array($value)) $this->sqlQuery->bindValue(":".$key, $value[0]);
                elseif($value != '' && !is_array($value)) $this->sqlQuery->bindValue(":".$key, $value);
            }

            $ex = $this->sqlQuery->execute();

            if($this->settings->debug === true && $ex === false || $this->settings->forceDebug) {
                $this->settings->errorInfo = [
                    'errorInfo' => $this->sqlQuery->errorInfo(),
                    'errorCode' => $this->sqlQuery->errorCode(),
                    'sqlQuery' => $q
                ];
            }

            if($connType === 'mysql') {
                $found = $this->doQuery()->query('SELECT FOUND_ROWS()');
                $this->settings->maxRows = $found->fetch(\PDO::FETCH_COLUMN);
            }else{
                $found = $this->doQuery()->query("SELECT COUNT(*) FROM {$this->properties->tableName}");
                $this->settings->maxRows = $found->fetch(\PDO::FETCH_COLUMN);
            }

            if($paginate > 0){
                if($this->settings->showNumbers === true) $this->settings->pagination->set_opt('showNumbers',true);
                $this->pagination()->setTotalRows($this->settings->maxRows);
                $this->pagination()->setRowsPerPage($paginate);
            }

        }catch(\Exception $e){
            if($this->settings->logDbTrace === true) logger::log(get_class($this), $e->getCode(), print_r($e->getMessage()),$this->settings->id,$this->db);
            $this->settings->response = new PDOResponse(false, 0, 'PDO Error - '.$e->getMessage());

            return $this->settings->response;
        }

        $i=0;
        $list=[];

        if((int)$this->settings->maxRows > 0){
            while($rs = $this->sqlQuery->fetch()){
                foreach($this->properties->fields AS $field)
                    $list[$i][$field] = $rs[$field];
                $list[$i][$this->properties->uniqueField] = $rs[$this->properties->uniqueField];
                $i++;
            }

            $this->settings->response = new PDOResponse(true, 1, ion\helpers\phraser::get()->translate('N_RECORDS_FOUND',false,[$this->settings->maxRows]), $list);
        }else{
            $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('NO_RECORDS_FOUND'));
        }

        return $this->settings->response;
    }

    /**
     * @param array $orderData
     * @return PDOResponse
     */
    public function order($orderData){
        $ids = implode(',', array_keys($orderData));
        $this->doQuery()->query("SELECT @i:=0");
        $query = "UPDATE {$this->properties->tableName} SET parentid = CASE id";
        foreach($orderData AS $key => $value)
            $query .= " WHEN ".$key." THEN ".$value;
        $query .= " END,";
        $query .= " orderid = @i:=@i+1";
        $query .= " WHERE ".$this->properties->uniqueField." IN (".$ids.")";
        $query .= " ORDER BY FIELD(id,".$ids.")";
        $ex = $this->doQuery()->query($query);

        if($ex){
            $this->sort_callback();
            $this->settings->response = new PDOResponse(true, 1, ion\helpers\phraser::get()->translate('SORT_SUCCESSFUL'));
        }else{
            $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('SORT_UNSUCCESSFUL'));
        }

        return $this->settings->response;
    }

    /**
     * @param $orderData
     * @return PDOResponse
     */
    public function order_flat($orderData){
        $ids = implode(',', array_keys($orderData));

        $this->doQuery()->query("SELECT @i:=0");
        $query = "UPDATE {$this->properties->tableName} SET";
        $query .= " orderid = @i:=@i+1";
        $query .= " WHERE ".$this->properties->uniqueField." IN (".$ids.")";
        $query .= " ORDER BY FIELD(id,".$ids.")";

        try {
            $this->doQuery()->query($query);
            $this->settings->response = new PDOResponse(true, 1, ion\helpers\phraser::get()->translate('SORT_SUCCESSFUL'));
        }catch(\PDOException $e){
            $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('SORT_UNSUCCESSFUL'));
        }

        $this->sort_callback();

        return $this->settings->response;
    }

    /**
     * @param string $column
     * @param mixed $value
     * @param bool $jsonEncode
     * @return $this
     */
    public function set_preset_value($column,$value,$jsonEncode=false){
        $curPreset = $this->settings->presetValues;

        if($jsonEncode === true)
            $value = json_encode($value,JSON_PRETTY_PRINT);

        $curPreset[$column] = $value;

        $this->settings->presetValues = $curPreset;

        return $this;
    }

    /**
     * @param string $phrase
     * @param array $fields
     * @param null|string $suffix
     * @param int $paginate
     * @param null|string $translate
     * @return PDOResponse
     */
    public function search($phrase,$fields,$suffix=null,$paginate=0,$translate=null){
        $fields = array_intersect($fields,$this->properties->fields);

        if($translate !== null) {
            $managing = ion\ion::get()->get_current_app()->get_setting('managing');
            $lang_list = ion\ion::get()->get_current_app(($managing === null) ? true : $managing)->get_setting('languages');
            array_shift($lang_list);

            foreach($lang_list AS $l)
                $lang_list[] = $l->short;

            if(in_array($translate,$lang_list)){
                $translator = new translate;
                $tr_fields = array_intersect($translator->get_fields(),$fields);
                $str_fields = [];

                foreach($tr_fields AS $field){
                    $str_fields[] = "translations." . $field;
                }
                $fields = $str_fields;

                $joinPhrase = " LEFT JOIN translations ON translations.content_table = '{$this->properties->tableName}' AND translations.content_id = {$this->properties->tableName}.id";
                $suffix = str_replace('{lang_status}',' OR translations.active = 1',$suffix);

                $prefix = ", ".implode(',',$str_fields);
            }else{
                $suffix = str_replace('{lang_status}',null,$suffix);
                $prefix = null;
                $joinPhrase = null;
            }
        }else{
            $suffix = str_replace('{lang_status}',null,$suffix);
            $prefix = null;
            $joinPhrase = null;
        }

        $connType = ion\ion::get()->db()->getConnectionInfo(CF_DOMAIN.'-'.$this->settings->connName)['type'];
        $counter = ($connType === 'mysql') ? ' SQL_CALC_FOUND_ROWS' : '';

        $q = "SELECT {$counter} {$this->properties->tableName}.*, MATCH (".implode(',',$fields).") AGAINST (:q) AS relevance ".$prefix." FROM ".$this->properties->tableName.$joinPhrase." WHERE MATCH (".implode(',',$fields).")";
        $q .= " AGAINST (:q IN BOOLEAN MODE) ".$suffix;

        if($paginate > 0){
            $start = ($_GET['page'] > 1) ? $paginate*($_GET['page']-1) : 0;
            $q .= " LIMIT {$start},".$paginate;

            $this->settings->pagination = new pagination;

            if($this->settings->showNumbers === true) $this->settings->pagination->set_opt('showNumbers',true);
        }

        $sql = $this->doQuery()->prepare($q);
        $sql->bindParam(':q',$phrase);

        $ex = $sql->execute();

        if($connType === 'mysql') {
            $found = $this->doQuery()->query('SELECT FOUND_ROWS()');
            $this->settings->maxRows = $found->fetch(\PDO::FETCH_COLUMN);
        }else{
            $found = $this->doQuery()->query("SELECT COUNT(*) FROM {$this->properties->tableName}");
            $this->settings->maxRows = $found->fetch(\PDO::FETCH_COLUMN);
        }

        if($paginate > 0){
            if($this->settings->showNumbers === true) $this->settings->pagination->set_opt('showNumbers',true);
            $this->pagination()->setTotalRows($this->settings->maxRows);
            $this->pagination()->setRowsPerPage($paginate);
        }

        $i=0;
        $list=array();

        if((int)$this->settings->maxRows > 0){
            while($rs = $sql->fetch()){
                foreach($this->properties->fields AS $field)
                    $list[$i][$field] = ($rs["translations_".$field] !== null && !in_array($rs["translations_".$field],["",null,"null","NULL","[]"])) ? $rs["translations_".$field] : $rs[$field];
                $list[$i]['id'] = $rs['id'];
                $i++;
            }
            $this->settings->response = new PDOResponse(true, 1, ion\helpers\phraser::get()->translate('N_RECORDS_FOUND',false,[$this->settings->maxRows]), $list);
        }else{
            $this->settings->response = new PDOResponse(false, 0, ion\helpers\phraser::get()->translate('NO_RECORDS_FOUND'));
        }

        return $this->settings->response;
    }

    /**
     * @return pagination
     */
    public function pagination(){
        return $this->settings->pagination;
    }

    protected function insert_callback(){}
    protected function update_callback(){}
    protected function delete_callback(){}

    protected function sort_callback(){}
}
