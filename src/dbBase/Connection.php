<?php
/**
 * Created by PhpStorm.
 * User: burak
 * Date: 04.11.2018
 * Time: 02:15
 */

namespace burakg\ion\dbBase;


abstract class Connection
{
    /**
     * @var mixed
     */
    public $connection;

    /**
     * @var string
     */
    public $host;

    /**
     * @var int
     */
    public $port;

    /**
     * @var string
     */
    public $databaseName;

    /**
     * @var null|string
     */
    public $user;

    /**
     * @var null|string
     */
    public $password;

    /**
     * @var mixed|null
     */
    public $options;

    /**
     * @var bool
     */
    public $status = false;

    /**
     * Connection constructor
     *
     * Provide a hostname
     * Provide a port number
     * Provide user and password (optional)
     * Provide a connection query string beginning with '?' (optional)
     *
     * @param string $host
     * @param int $port
     * @param string $databaseName
     * @param null|string $user
     * @param null|string $password
     * @param null|mixed $options
     */
    final public function __construct($host, $port, $databaseName, $user = null, $password = null, $options = null)
    {
        $this->host = $host;
        $this->port = $port;
        $this->databaseName = $databaseName;
        $this->user = $user;
        $this->password = $password;
        $this->options = $options;
    }

    /**
     * @return mixed
     */
    abstract public function connect();

    /**
     * @param $name
     * @param $value
     */
    final public function __set($name, $value)
    {}
}