<?php

namespace burakg\ion\dbBase;
use burakg\ion AS ion;

class mediumManager extends ion\dbBase {
	use ion\dbBase\dbBaseTrait;

	protected function init() {
		$targetApp = (string)ion\ion::get()->get_current_app()->get_setting('managing');
		$targetAppName = ion\helpers::get()->text($targetApp)->substitute(true);
		$this->properties->uploadSrc = ion\ion::get()->get_current_app($targetAppName)->CB_ASSETS.'uploads/';
		$this->properties->uploadSrcFront = ion\ion::get()->get_current_app($targetAppName)->CF_ASSETS.'uploads/';
		$this->properties->tableName = 'mediums';
		$this->properties->fields = [
			'filename',
			'file_ext',
			'content_id',
			'content_table',
			'user_id',
			'attachment',
			'extra_data'
		];
		$this->properties->nameIndex = [];
		$this->properties->idIndex = [];

		$this->settings->extraFields = [];
		$this->settings->presetValues = [];
		$this->settings->writeDB = false;
		$this->settings->rename = false;
		$this->settings->inputName = 'file_single';
		$this->settings->folder = $this->properties->uploadSrc;
		$this->settings->extension = null;

		if(!is_dir($this->settings->folder))
			mkdir($this->settings->folder);
	}

	public function upload($attachment=0){
		$this->properties->count = ($_FILES[$this->settings->inputName]['name'][0] != '') ? count($_FILES[$this->settings->inputName]['name']) : 0;

		for($i=0;$i<$this->properties->count;$i++){
			$this->properties->name = $_FILES[$this->settings->inputName]['name'][$i];
			$this->properties->tmp_name = $_FILES[$this->settings->inputName]['tmp_name'][$i];
			$this->properties->currentExtension = ($this->settings->extension !== null) ? $this->settings->extension : strrchr($this->properties->name, '.');

			$presetValues = [];
			foreach($this->settings->extraFields AS $key => $val){
				$presetValues[$key] = $this->settings->extraFields[$key][$i];
			}
			$this->settings->presetValues = $presetValues;
			if($this->settings->writeDB === true) $this->write_db($attachment);

			if(count($this->properties->idIndex) > 0 && $this->settings->rename === false){
				$prefix = ($attachment == 1) ? '_attachment_' : '_visual_';
				$this->properties->newName = $this->settings->contentTable.$prefix.$this->properties->idIndex[$i].$this->properties->currentExtension;
			}elseif($this->settings->rename !== false && count($this->properties->idIndex) == 0){
				$this->properties->newName = $this->settings->rename.$this->properties->currentExtension;
			}else{
				$this->properties->newName = ($this->properties->file_convert_seo === true) ? ion\helpers::get()->text(str_replace(['.jpg','.gif','.png'],'',$this->properties->name))->slug().$this->properties->currentExtension : $this->properties->name.$this->properties->currentExtension;
			}

			$this->properties->nameIndex = array_merge($this->properties->nameIndex,[$this->properties->newName]);

			if(file_exists($this->settings->folder.$this->properties->newName)) unlink($this->settings->folder.$this->properties->newName);
			move_uploaded_file($this->properties->tmp_name, $this->settings->folder.$this->properties->newName);
		}

		return ['name_index' => $this->properties->nameIndex, 'id_index' => $this->properties->idIndex];
	}

	public function write_db($attachment=0){
		$extradata = json_encode($this->settings->presetValues);
		$this->settings->presetValues = [
			'filename' => $this->properties->name,
			'file_ext' => strrchr($this->properties->name,'.'),
			'content_id' => $this->settings->contentId,
			'content_table' => $this->settings->contentTable,
			'user_id' => ion\front\auth::get()->get_user()['id'],
			'attachment' => $attachment,
			'extra_data' => $extradata
		];

		$this->save();
		$this->settings->presetValues = [];

		return $this->settings->id;
	}

    /**
     * @param bool $returnRaw
     * @return PDOResponse
     */
	public function data_by_id($returnRaw=false){
		$data_list = $this->get_list("",false,['content_table' => $this->settings->contentTable, 'content_id' => $this->settings->contentId]);
		if($returnRaw===true) return $data_list;

		if($data_list->status){
            $output = [];

            foreach($data_list->data AS $data){
				$file = $data['content_table'].'_'.$data['id'].strrchr($data['filename'], ".");
				$output[] = [$file,$data['id']];
			}

            $this->settings->response = new PDOResponse($data_list->status, $data_list->code, $data_list->message, $output);

            return $this->settings->response;
		}

		return $data_list;
	}

    /**
     * @param $content_id
     * @param string $content_table
     * @param array $size
     * @param bool $tagged
     * @return PDOResponse
     */
	public function images_by_id($content_id,$content_table='sitemap',$size=[50,50],$tagged=true){
		$data_list = $this->get_list("",false,['content_table' => $content_table, 'content_id' => $content_id]);

		if($data_list->status){
            $output = [];

            foreach($data_list->data AS $data){
				$size_prefix = (count($size) == 2) ? implode('x', $size).'_' : "";
				$img = ($tagged === true) ? ion\ion::get()->helpers()->html('')->show_image($data['content_table'].'_'.$size_prefix.$data['id'].strrchr($data['filename'], ".")) : $data['content_table'].'_'.$size_prefix.$data['id'].strrchr($data['filename'], ".");
				$output[] = [$img,$data['id']];
			}

            $this->settings->response = new PDOResponse($data_list->status, $data_list->code, $data_list->message, $output);

            return $this->settings->response;
		}

		return $data_list;
	}

    /**
     * @param $content_id
     * @param string $content_table
     * @param array $size
     * @param bool $tagged
     * @return PDOResponse
     */
	public function sized_images_by_id($content_id,$content_table='sitemap',$size=[50,50],$tagged=true){
		$data_list = $this->add_db_field('added_date')->get_list(null,0,['content_table' => $content_table, 'content_id' => $content_id]," AND lower(file_ext) IN('.jpg','.jpeg','.gif','.png') AND attachment = 0 ORDER BY orderid ASC");

		if($data_list->status){
            $output = [];

            foreach($data_list->data AS $data){
				$size_prefix = (count($size) >= 2) ? $size[0].'x'.$size[1].'_' : "";
				$img = ($tagged === true) ? ion\ion::get()->helpers()->html('')->show_image($size_prefix.$data['content_table'].'_visual_'.$data['id'].$data['file_ext'],['alt' => 'image-id-'.$data['id']]) : $size_prefix.$data['content_table'].'_visual_'.$data['id'].$data['file_ext'];
				$output[] = [
					'id' => $data['id'],
					'source' => $img,
					'extension' => $data['file_ext'],
					'raw-source' => $data['content_table'].'_visual_'.$data['id'].$data['file_ext'],
					'date-added' => $data['added_date'],
					'extra-data' => $data['extra_data']
				];
			}

			$this->settings->response = new PDOResponse($data_list->status, $data_list->code, $data_list->message, $output);

			return $this->settings->response;
		}

		return $data_list;
	}

    /**
     * @param $content_id
     * @param string $content_table
     * @return PDOResponse
     */
	public function attachments_by_id($content_id,$content_table='sitemap'){
		$data_list = $this->get_list(null,0,['content_table' => $content_table, 'content_id' => $content_id, "attachment" => 1]," ORDER BY orderid ASC, id ASC");

        if($data_list->status){
            $output = [];

            foreach($data_list->data AS $data){
				$extra_data = json_decode($data['extra_data'],true);
				$output[] = [
					'id' => $data['id'],
					'extension' => $data['file_ext'],
					'raw-source' => $data['content_table'].'_attachment_'.$data['id'].$data['file_ext'],
					'date-added' => $data['added_date'],
					'extra-data' => $extra_data,
					'content-table' => $data['content_table'],
					'filename' => $data['filename']
				];
			}

            $this->settings->response = new PDOResponse($data_list->status, $data_list->code, $data_list->message, $output);

            return $this->settings->response;
		}

		return $data_list;
	}

	protected function insert_callback(){
		$this->properties->idIndex = array_merge($this->properties->idIndex,[$this->settings->id]);
		$this->settings->id = 0;
	}

	public function delete_callback() {
		$this->delete_files($this->settings->id);
	}

	public function delete_files($id){
		$query = "SELECT * FROM mediums WHERE id = ".$id;
		$sql = $this->properties->dbConn->query($query);
		$rs = $sql->fetch();
		array_map('unlink', glob($this->properties->uploadSrc.'*_visual_'.$id.$rs['file_ext']));
//		$glob = glob($this->properties->uploadSrc.'*_{'.$id.'}'.$rs['file_ext']);

//		if(is_array($glob))
//			foreach($glob AS $delete_me)
//				@unlink($delete_me);

		@unlink($this->properties->uploadSrc.'/'.$rs['id'].strrchr($rs['filename'], "."));

		return 'ION.callBacks.removeRow("med_'.$id.'");';
	}

	public function bulk_delete($content_id,$content_table='sitemap'){
		$query = "SELECT * FROM mediums WHERE content_id = ".$content_id." AND content_table = '".$content_table."'";
		$sql = $this->doQuery()->query($query);
		while($rs = $this->properties->dbConn->fetch())
			$this->delete_files($rs['id']);
	}

	public function delete_by_content_id($id=0,$prefix=null){
		if($id > 0){
			if(is_array($this->settings->resize_sizes)){
				foreach($this->settings->resize_sizes AS $size)
					@unlink($this->settings->folder.'/'.$prefix.$size[0].'x'.$size[1].'_'.$id.$this->settings->img_ext);
			}
		}
	}

}
