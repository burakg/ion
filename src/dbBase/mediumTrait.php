<?php
/**
 * Class mediumBundle
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 26.05.2015
 */

namespace burakg\ion\dbBase;

use burakg\ion AS ion;
use Zebra_Image;

trait mediumTrait {
	protected function add_medium(){
		$this->add_medium_by_id();
		$this->add_medium_multiple();
		$this->add_attachment_by_id();
	}

	public function add_attachment_by_id(){
		$medium = new mediumManager;

		if($_FILES['file_attachment']['name'][0]){
			$targetApp = (string)ion\ion::get()->get_current_app()->get_setting('managing');
			$targetAppName = ion\helpers::get()->text($targetApp)->substitute(true);
			$srcFolder = ion\ion::get()->get_current_app($targetAppName)->CB_ASSETS.'uploads/';

			$medium->writeDB = true;
			$medium->contentId = $this->settings->id;
			$medium->contentTable = $this->properties->tableName;
			$medium->folder = $srcFolder;
			$medium->inputName = 'file_attachment';
			$presetValues = [];
			foreach ($_POST as $key => $value) {
				if (strpos($key, 'file_attachment_') === 0) {
					$presetValues[$key] = $value;
				}
			}
			$medium->extraFields = $presetValues;

			$file_return = $medium->upload(1);
		}elseif($_FILES['file_attachment']['name'][0] == null && ion\helpers::get()->validate($_POST['attachment_title'])->is_null() === false){
			$extradata = [];
			foreach ($_POST as $key => $value) {
				if (strpos($key, 'attachment_') === 0) {
					$extradata[$key] = $value;
				}
			}

			$medium->presetValues = array(
				'filename' => null,
				'file_ext' => null,
				'folder' => null,
				'content_id' => $this->settings->id,
				'content_table' => $this->properties->tableName,
				'user_id' => ion\front\auth::get()->get_user()['id'],
				'attachment' => 1,
				'extra_data' => json_encode($extradata)
			);

			$medium->save();
		}

		return $file_return;
	}

	public function resize_image($filename,$ext){
        $targetApp = (string)ion\ion::get()->get_current_app()->get_setting('managing');
        $targetAppName = ion\helpers::get()->text($targetApp)->substitute(true);
        $srcFolder = ion\ion::get()->get_current_app($targetAppName)->CB_ASSETS.'uploads/';

        $resizer = new Zebra_Image;
        $resizer->source_path = $srcFolder.$filename;
        if($ext !== NULL){
            $resizer->target_path = $srcFolder.(str_replace(['.jpg','.gif','.png'], $ext, $filename));
            $resizer->resize(0,0,ZEBRA_IMAGE_NOT_BOXED,$this->settings->pngBG);
        }
        foreach($this->settings->resizeSizes AS $size){
            $resizer->target_path = ($ext === NULL) ? $srcFolder.$size[0].'x'.$size[1].'_'.$filename : $srcFolder.$size[0].'x'.$size[1].'_'.(str_replace(['.jpg','.gif','.png'], $ext, $filename));

            if($this->settings->cropFromCenter === true || $size[2] === true){
                $resizer->resize($size[0],$size[1], ZEBRA_IMAGE_CROP_CENTER, $size[3]);
            }elseif($size[2] === 'expand'){
                $resizer->resize($size[0],$size[1], ZEBRA_IMAGE_BOXED, $size[3]);
            }else{
                $resizer->resize($size[0],$size[1], ZEBRA_IMAGE_NOT_BOXED, $size[3]);
            }
        }
        if($this->settings->resizeOriginal !== false && is_array($this->settings->resizeOriginal)){
            unset($resizer);
            $resizer = new Zebra_Image;
            $resizer->source_path = $srcFolder.$filename;
            $resizer->target_path = $srcFolder.$filename;

            if($this->settings->resizeOriginal[2] === true)
                $resizer->resize($this->settings->resizeOriginal[0],$this->settings->resizeOriginal[1], ZEBRA_IMAGE_CROP_CENTER, $this->settings->resizeOriginal[3]);
            elseif($this->settings->resizeOriginal[2] === 'expand')
                $resizer->resize($this->settings->resizeOriginal[0],$this->settings->resizeOriginal[1], ZEBRA_IMAGE_BOXED, $this->settings->resizeOriginal[3]);
            else
                $resizer->resize($this->settings->resizeOriginal[0],$this->settings->resizeOriginal[1], ZEBRA_IMAGE_NOT_BOXED, $this->settings->resizeOriginal[3]);
        }
    }

	protected function add_medium_by_id($prefix="",$inputName="file_single",$fileRename=NULL,$ext=NULL){
		$medium = new mediumManager;
		$medium->rename = ($fileRename === NULL) ? $this->properties->tableName.'_main_'.$prefix.$this->settings->id : $fileRename;
		$medium->inputName = $inputName;

		$file_return = $medium->upload();

		if($file_return['name_index'][0] != ''){
			$this->resize_image($file_return['name_index'][0],$ext);
		}
	}

	protected function add_medium_multiple($overrideTable=false,$inputName='file_multiple'){
		$medium = new mediumManager;
		$medium->writeDB = true;
		$medium->contentId = $this->settings->id;
		$medium->contentTable = ($overrideTable === false) ? $this->properties->tableName : $overrideTable;

		$medium->inputName = $inputName;
		$medium->defaultImageFormat = $this->settings->defaultImageFormat;
		$medium->mediumLimit = $this->settings->mediumLimit;

		$file_return = $medium->upload();

		if(is_array($file_return['name_index']) && $file_return['name_index'][0] != ''){
			for($i=0;$i<count($file_return['name_index']);$i++){
				$this->resize_image($file_return['name_index'][$i],null);
				if($this->settings->mediumLimit > 0 && $i == ($this->settings->mediumLimit-1))
					break;
			}
		}
	}

	public function get_images($controls=true,$size=[50,50],$tagged=true,$overrideTable=false){
		if($this->settings->id > 0){
			$medium = new mediumManager;
			$medium->add_db_field('orderid');
			$medium->contentId = $this->settings->id;
			$medium->contentTable = ($overrideTable === false) ? $this->properties->tableName : $overrideTable;
			$data = $medium->sized_images_by_id($this->settings->id,$medium->contentTable,$size,$tagged);

			$targetApp = (string)ion\ion::get()->get_current_app()->get_setting('managing');
			$targetAppName = ion\helpers::get()->text($targetApp)->substitute(true);
			$srcFolder = ion\ion::get()->get_current_app($targetAppName)->CB_ASSETS.'uploads/';

			$output = [];
			if($data->status){
				if($controls === true){
					$i=0;

					foreach($data->data AS $data_item){
                        $output[$i] = ion\helpers\template::get()->apply('bound-image',[
							'image' => $data_item['source'],
							'node-id' => $data_item['id'],
							'edit-link' => ion\front\navigation::get()->node_url_by_attr('id',14).'?id='.$data_item['id'],
							'edit-title' => ion\helpers\phraser::get()->translate('CROP_IMAGE'),
							'crop-title' => null,
							'remove-link' => ion\ion::get()->get_current_app()->CF_ROOT.'ajax-handler/medium/remove?id='.$data_item['id'],
							'remove-title' => ion\helpers\phraser::get()->translate('REMOVE'),
							'file-mdate' => ion\helpers::get()->data($data_item['date-added'])->readable_date(),
							'file-size' => ion\helpers::get()->data(filesize($srcFolder.$data_item['raw-source']))->readable_size(),
							'label-filesize' => ion\helpers\phraser::get()->translate('FILE_SIZE'),
							'label-added-date' => ion\helpers\phraser::get()->translate('DATE_ADDED'),
							'label-file-extension' => ion\helpers\phraser::get()->translate('FILE_EXTENSION'),
							'file-extension' => strtoupper($data_item['extension']),
							'raw-source' => ion\ion::get()->get_current_app($targetAppName)->CF_ASSETS.'uploads/'.$data_item['raw-source']
						]);
						$i++;
					}

				}elseif($controls === 'sort'){
                    $i=0;

                    foreach($data->data AS $data_item){
                        $output[$i] = ion\helpers\template::get()->apply('image-grid',[
                            'node-id' => $data_item['id'],
                            'raw-source' => ion\ion::get()->get_current_app($targetAppName)->CF_ASSETS.'uploads/'.$data_item['raw-source']
                        ]);
                        $i++;
                    }

                }else{
				    $output = $data->data;
                }
				return $output;
			}else{
                return ['<tr><td colspan="4">'.ion\helpers\phraser::get()->translate('NO_RECORDS_FOUND').'</td></tr>'];
            }
		}
        return ['<tr><td colspan="4">'.ion\helpers\phraser::get()->translate('NO_RECORDS_FOUND').'</td></tr>'];
	}

	public function get_attachments($controls=true){
		$app = ion\ion::get()->get_current_app();
		$this->settings->icons = [
			'pdf' => 'fa-file-pdf-o',
			'xls' => 'fa-file-excel-o',
			'xlsx' => 'fa-file-excel-o',
			'ods' => 'fa-file-excel-o',
			'doc' => 'fa-file-word-o',
			'docx' => 'fa-file-word-o',
			'rtw' => 'fa-file-word-o',
			'odt' => 'fa-file-word-o',
			'ppt' => 'fa-file-powerpoint-o',
			'pptx' => 'fa-file-powerpoint-o',
			'odp' => 'fa-file-powerpoint-o',
			'rar' => 'fa-file-archive-o',
			'zip' => 'fa-file-archive-o',
			'7z' => 'fa-file-archive-o',
			'tar' => 'fa-file-archive-o',
			'gz' => 'fa-file-archive-o',
			'jpg' => 'fa-file-image-o',
			'jpeg' => 'fa-file-image-o',
			'jpe' => 'fa-file-image-o',
			'png' => 'fa-file-image-o',
			'gif' => 'fa-file-image-o',
			'mpg' => 'fa-file-video-o',
			'mp4' => 'fa-file-video-o',
			'avi' => 'fa-file-video-o',
			'webm' => 'fa-file-video-o',
			'wmv' => 'fa-file-video-o',
			'mp3' => 'fa-file-audio-o',
			'ogg' => 'fa-file-audio-o',
			'aac' => 'fa-file-audio-o',
			'wav' => 'fa-file-audio-o',
			'wma' => 'fa-file-audio-o',
		];

		if($this->settings->id > 0){
			$medium = new mediumManager;
			$dataList = $medium->attachments_by_id($this->settings->id,$this->properties->tableName);
			$fn = function(&$n) use($app){
				$a = $n;
				$extension = end(explode('.',$a['filename']));
				$extra = $a['extra_data'];
				$icon = (array_key_exists($extension,$this->settings->icons)) ? $this->settings->icons[$extension] : 'fa-file';
				$n = '
			<li class="list-group-item" id="row_'.$a['id'].'">
				<a href="'.$app->CF_ASSETS.'uploads/'.$a['raw-source'].'" target="_blank"><span class="fa '.$icon.'"></span> '.$a['extra_data']['title'].' '.$a['filename'].'</a>
				<a href="'.$app->CF_ROOT.'medium/delete?id='.$a['id'].'" class="ajax-me confirm-me badge alert-danger" title="Sil"><i class="fa fa-times"></i></a>
			</li>';};
			if($dataList->status){
			    $data = $dataList->data;
				if($controls === true) array_walk($data, $fn);
				return new PDOResponse($dataList->status, $dataList->code, $dataList->message, $data);
			}else{
				return $dataList;
			}
		}
		return [];
	}

	/**
	 * @param array $size
	 * @param array $attributes
	 * @param mixed $alternate
	 * @param bool $tag
	 * @param bool $forceCurrentApp
	 * @return null|string
	 */
	public function show_main_image($size=[150,150],$attributes=[],$alternate=null,$tag=true,$forceCurrentApp=false){
		if($this->settings->id > 0 && ion\helpers::get()->validate($this->settings->details['imgext'])->is_null() === false){
			$size_prefix = (count($size) >= 2) ? $size[0].'x'.$size[1].'_' : "";
			return ion\helpers::get()->html(null)->show_image(
				$size_prefix.$this->properties->tableName.'_main_'.$this->settings->id.$this->settings->details['imgext'],
				$attributes,
				$alternate,
				$tag,
				$forceCurrentApp
			);
		}elseif($alternate !== null){
			return ($tag) ? '<img src="'.$alternate.'" />' : $alternate;
		}
		return null;
	}

	protected function remove_medium_by_id(){
		$data = new mediumManager;
		$data->contentId = $this->settings->id;
		$data->contentTable = $this->properties->tableName;
		$data->delete_by_content_id($this->settings->id);
	}
}