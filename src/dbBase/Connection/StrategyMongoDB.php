<?php
/**
 * Mongo strategy for ION Engine DB interface
 * To use this strategy, your project must require mongodb/mongodb library
 * You can do this by typing:
 *
 * composer require mongodb/mongodb
 */

namespace burakg\ion\dbBase\Connection;

use burakg\ion\dbBase\Connection;
use MongoDB\Client;

class StrategyMongoDB extends Connection
{

    /**
     * StrategyMongoDB constructor.
     *
     * @return Client|string
     */
    public function connect()
    {
        $connString = ($this->user !== null && $this->password !== null) ?
            "mongodb://".$this->user.":".$this->password."@".$this->host.":".$this->port."/".$this->databaseName :
            "mongodb://".$this->host.":".$this->port.$this->port."/".$this->databaseName;

        try {
            $this->connection = new Client($connString,(array)$this->options);

            $this->status = true;

            return $this->connection;
        }catch (\Exception $e){
            $this->status = false;

            return $e->getMessage();
        }
    }

}