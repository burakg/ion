<?php
/**
 * Class mongoBaseTrait
 */
namespace burakg\ion\dbBase;

use burakg\ion\helpers\phraser;
use burakg\ion\ion;
use burakg\logger\logger;
use MongoDB\BSON\ObjectId;
use MongoDB\Collection;
use MongoDB\Driver\Exception\RuntimeException;
use MongoDB\Exception\UnsupportedException;
use MongoDB\Exception\InvalidArgumentException;

// Todo: Implement a log mechanism

trait MongoDBTrait {
    /**
     * @var string
     */
    public $connName;
    /**
     * @var bool|\MongoDB\Client
     */
    public $dbConn;
    /**
     * @var string
     */
    public $uniqueField = '_id';
    /**
     * @var array
     */
    public $schema = [];
    /**
     * @var Collection
     */
    public $collection;

    /**
     * @var int|string|null
     */
    public $id = null;

    /**
     * Set true if the unique identifier field type is a BSON ObjectId
     * @var bool
     */
    protected $isObjectID = true;

    /**
     * @var array|null|object
     */
    public $cursor;

    /**
     * @var array
     */
    public $presetValues = [];

    /**
     * @var MongoResponse
     */
    public $response;

    /**
     * @var string
     */
    public $collectionName;

    /**
     * Returns the pagination object
     *
     * @var pagination
     */
    public $pagination;

    /**
     * MongoDBTrait constructor.
     * @param string $connectionName
     */
    public function __construct($connectionName='mongo')
    {
        $this->connName = $connectionName;

        try{
            $this->dbConn = ion::get()->db()->initConns()->connectMongo($connectionName);

            $this->init();

            $this->collection = $this->dbConn->selectCollection($this->collectionName);
        }
        catch(UnsupportedException $e){
            $this->response = new MongoResponse(false, 0, 'Connection failed: '. $e->getMessage());
        }
        catch(InvalidArgumentException $e){
            $this->response = new MongoResponse(false, 0, 'Connection failed: '. $e->getMessage());
        }
        catch(RuntimeException $e){
            $this->response = new MongoResponse(false, 0, 'Connection failed: '. $e->getMessage());
        }
    }

    /**
     * Magic getter method
     *
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->$key;
    }

    public function __set($key,$val){}

    /**
     * Get the current collection
     *
     * @return Collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * Get name of the current connection
     *
     * @return string
     */
    public function getConnName()
    {
        return $this->connName;
    }

    /**
     * Do a custom query by accessing the MongoDB Client object
     *
     * @return bool|\MongoDB\Client
     */
    public function doQuery()
    {
        return $this->dbConn;
    }

    /**
     * Get schema array
     *
     * @return array
     */
    public function getSchema()
    {
        return $this->schema;
    }

    /**
     * Get the name of the unique identifier field
     *
     * @return string
     */
    public function getUniqueField()
    {
        return $this->uniqueField;
    }

    /**
     * Set the unique id of the instance
     *
     * @param int|string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = ($this->isObjectID) ? new ObjectId($id) : $id;

        return $this;
    }

    /**
     * Set the preset values for document insert / update
     * 
     * @param array $data
     * @return $this
     */
    public function setPresetValues($data)
    {
        $this->presetValues = $data;

        return $this;
    }

    /**
     * Triggers either update or insert methods depending on $id property value
     * If $id property of the instance is null, the update method will be called
     * Else, the insert method will be called
     *
     * @return $this
     */
    public function save(){
        if($this->id === null){
            $this
                ->insert()
                ->insertHook();
        }else{
            $this
                ->update()
                ->updateHook();
        }
        return $this;
    }

    /**
     * Update a collection in the collection
     *
     * @return $this
     */
    private function update()
    {
        if($this->id === null){
            $this->response = new MongoResponse(false, 0, 'The unique id must be different than "null" to update a document');
            return $this;
        }

        try {
            $update = $this->collection->replaceOne([$this->uniqueField => $this->id], $this->presetValues);

            $upsertCount = $update->getModifiedCount();

            if($upsertCount > 0){
                $this->cursor = $this->collection->findOne([$this->uniqueField => $update->getUpsertedId()]);
                $this->response = new MongoResponse(
                    true, 1,
                    phraser::get()->translate('UPDATE_SUCCESSFUL'),
                    $this->cursor
                );

                //logger::log(get_class($this), 1, 'Update success', $this->collectionName, $this->id);
            }else{
                $errorMessage = ($update->getUpsertedCount() == 0) ? 'No Matching Records Found' : 'No Changes were Made';
                //logger::log(get_class($this), 1, 'Update failed: '.$errorMessage, $this->collectionName, $this->id);
                $this->response = new MongoResponse(false, 0, $errorMessage);
            }
        }
        catch(UnsupportedException $e){
            //logger::log(get_class($this), 1, 'Update failed: '.$e->getMessage(), $this->collectionName, $this->id);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('UPDATE_UNSUCCESSFUL').'-'.$e->getMessage());
        }
        catch(InvalidArgumentException $e){
            //logger::log(get_class($this), 1, 'Update failed: '.$e->getMessage(), $this->collectionName, $this->id);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('UPDATE_UNSUCCESSFUL').'-'.$e->getMessage());
        }
        catch(RuntimeException $e){
            //logger::log(get_class($this), 1, 'Update failed: '.$e->getMessage(), $this->collectionName, $this->id);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('UPDATE_UNSUCCESSFUL').'-'.$e->getMessage());
        }

        return $this;
    }

    /**
     * Insert a new document record to the collection
     *
     * @return $this
     */
    private function insert(){
        try{
            $insert = $this->collection->insertOne($this->presetValues);

            if($insert->getInsertedCount() > 0){
                $this->setId($insert->getInsertedId());

                $this->cursor = $this->collection->findOne([$this->uniqueField => $this->id]);

                $this->response = new MongoResponse(
                    true, 1,
                    phraser::get()->translate('INSERT_SUCCESSFUL'),
                    $this->cursor
                );
                //logger::log(get_class($this), 1, 'Insert success', $this->collectionName, $this->id);
            }else{
                //logger::log(get_class($this), 1, 'Insert failed: Unknown error', $this->collectionName, $this->id);
                $this->response = new MongoResponse(false, 0, phraser::get()->translate('INSERT_UNSUCCESSFUL'));
            }
        }
        catch(UnsupportedException $e){
            //logger::log(get_class($this), 1, 'Insert failed: '.$e->getMessage(), $this->collectionName, 0);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('INSERT_UNSUCCESSFUL'));
        }
        catch(InvalidArgumentException $e){
            //logger::log(get_class($this), 1, 'Insert failed: '.$e->getMessage(), $this->collectionName, 0);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('INSERT_UNSUCCESSFUL'));
        }
        catch(RuntimeException $e){
            //logger::log(get_class($this), 1, 'Insert failed: '.$e->getMessage(), $this->collectionName, 0);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('INSERT_UNSUCCESSFUL'));
        }

        return $this;
    }

    /**
     * Delete a document from the collection
     *
     * @return $this
     */
    public function delete(){
        if($this->id === null){
            $this->response = new MongoResponse(false, 0, 'The unique id must be different than "null" to delete a document');
            return $this;
        }
        try{
            $delete = $this->collection->deleteOne([$this->uniqueField => $this->id]);
            if($delete->getDeletedCount() == 1){
                $this->response = new MongoResponse(
                    true, 1,
                    phraser::get()->translate('DELETE_SUCCESSFUL')
                );

                //logger::log(get_class($this), 1, 'Delete Successful', $this->collectionName, $this->id);

                $this->deleteHook();
            }else{
                $this->response = new MongoResponse(
                    false, 0,
                    phraser::get()->translate('DELETE_UNSUCCESSFUL')
                );

                //logger::log(get_class($this), 1, 'Delete Failed: No matching records with the given unique identifier', $this->collectionName, $this->id);
            }

        }
        catch(UnsupportedException $e){
            //logger::log(get_class($this), 1, 'Delete failed: '.$e->getMessage(), $this->collectionName, 0);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('DELETE_UNSUCCESSFUL'));
        }
        catch(InvalidArgumentException $e){
            //logger::log(get_class($this), 1, 'Delete failed: '.$e->getMessage(), $this->collectionName, 0);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('DELETE_UNSUCCESSFUL'));
        }
        catch(RuntimeException $e){
            //logger::log(get_class($this), 1, 'Delete failed: '.$e->getMessage(), $this->collectionName, 0);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('DELETE_UNSUCCESSFUL'));
        }
        return $this;
    }

    /**
     * Update a boolean or binary field of a document with the opposite value
     *
     * @param string $field
     * @return $this
     */
    public function switchField($field='status'){
        if($this->id === null){
            $this->response = new MongoResponse(false, 0, 'The unique id must be different than "null" to delete a document');
            return $this;
        }

        try{
            $cursor = $this->collection->findOne([$this->uniqueField => $this->id]);

            if($cursor === null){
                $this->response = new MongoResponse(false, 0, 'Switch Failed: No matching records with the given unique identifier');
                return $this;
            }

            if($cursor->{$field} === 0){
                $newValue = 1;
            }elseif($cursor->{$field} === 1){
                $newValue = 0;
            }elseif($cursor->{$field} === false){
                $newValue = true;
            }elseif($cursor->{$field} === true){
                $newValue = false;
            }else{
                $this->response = new MongoResponse(false, 0, 'Switch Failed: The field is not a boolean or binary one');
                return $this;
            }

            $cursor = $this->collection->updateOne([$this->uniqueField => $this->id], [$field => $newValue]);

            if($cursor->getModifiedCount() == 1){
                $this->response = new MongoResponse(
                    true, 1,
                    phraser::get()->translate('SET_STATE_SUCCESSFUL')
                );
                //logger::log(get_class($this), 1, $field.' state set to '.$newValue, $this->collectionName, $this->id);
            }else{
                $this->response = new MongoResponse(
                    true, 1,
                    phraser::get()->translate('SET_STATE_UNSUCCESSFUL')
                );
            }
        }
        catch(UnsupportedException $e){
            //logger::log(get_class($this), 1, 'Delete failed: '.$e->getMessage(), $this->collectionName, 0);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('DELETE_UNSUCCESSFUL'));
        }
        catch(InvalidArgumentException $e){
            //logger::log(get_class($this), 1, 'Delete failed: '.$e->getMessage(), $this->collectionName, 0);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('DELETE_UNSUCCESSFUL'));
        }
        catch(RuntimeException $e){
            //logger::log(get_class($this), 1, 'Delete failed: '.$e->getMessage(), $this->collectionName, 0);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('DELETE_UNSUCCESSFUL'));
        }

        return $this;
    }

    /**
     * Get a single document by unique identifier field
     * The unique id must be set before executing this method
     *
     * @return $this
     */
    public function getOne()
    {
        if($this->id === null){
            $this->response = new MongoResponse(false, 0, 'The unique id cannot be null, please set id');
            return $this;
        }

        try {
            $document = $this->collection->findOne([$this->uniqueField => $this->id]);

            if($document === null){
                $this->response = new MongoResponse(false, 0, phraser::get()->translate('NO_RECORDS_FOUND'));
            }else{
                $this->response = new MongoResponse(true, 1, null, $document);
            }
        }
        catch(UnsupportedException $e){
            //logger::log(get_class($this), 1, 'Find failed: '.$e->getMessage(), $this->collectionName, $this->id);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('QUERY_FAILED'));
        }
        catch(InvalidArgumentException $e){
            //logger::log(get_class($this), 1, 'Delete failed: '.$e->getMessage(), $this->collectionName, $this->id);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('QUERY_FAILED'));
        }
        catch(RuntimeException $e){
            //logger::log(get_class($this), 1, 'Delete failed: '.$e->getMessage(), $this->collectionName, $this->id);
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('QUERY_FAILED'));
        }

        return $this;
    }

    /**
     * @param array $filter
     * @param int $paginate
     * @param array $order
     * @return $this
     */
    public function find($filter=[], $paginate=0, $order=[])
    {
        try {
            $options = [
                'limit' => $paginate
            ];
            if(count($order) > 0)
                $options['order'] = $order;

            if($paginate > 0){
                $start = ($_GET['page'] > 1) ? $paginate*($_GET['page']-1) : 0;
                $options['skip'] = $start;
                $options['limit'] = $start+$paginate;
            }

            $cursor = $this->collection->find($filter, $options)->toArray();
            $count = count($cursor);

            if($count > 0){
                $this->response = new MongoResponse(true, 0, phraser::get()->translate('N_RECORDS_FOUND', false, [$count]), $cursor);

                if($paginate > 0){
                    $this->pagination = new pagination;
                    $this->pagination->setTotalRows($count);
                    $this->pagination->setRowsPerPage($paginate);
                }
            }else{
                $this->response = new MongoResponse(false, 0, phraser::get()->translate('NO_RECORDS_FOUND'), []);
            }
        }
        catch(UnsupportedException $e){
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('QUERY_FAILED'));
        }
        catch(InvalidArgumentException $e){
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('QUERY_FAILED'));
        }
        catch(RuntimeException $e){
            $this->response = new MongoResponse(false, 0, phraser::get()->translate('QUERY_FAILED'));
        }

        return $this;
    }

    protected function insertHook(){}
    protected function updateHook(){}
    protected function deleteHook(){}
}