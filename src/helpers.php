<?php
/**
 *
 * Class helpers
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 13.12.2014
 *
 */

namespace burakg\ion;

class helpers {
	use singletonTrait;

	protected $textHelper, $numberHelper, $htmlHelper, $validateHelper, $dataHelper;

	protected function __construct(){
		$this->textHelper = helpers\text::get();
		$this->numberHelper = helpers\numeric::get();
		$this->htmlHelper = helpers\html::get();
		$this->validateHelper = helpers\validate::get();
		$this->dataHelper = helpers\data::get();
	}

	/**
	 * @param $var
	 * @return helpers\text
	 */
	public function text($var){
		return $this->textHelper->set($var);
	}

	/**
	 * @param $var
	 * @return helpers\numeric
	 */
	public function number($var){
		return $this->numberHelper->set($var);
	}

	/**
	 * @param $var
	 * @return helpers\html
	 */
	public function html($var=null){
		return $this->htmlHelper->set($var);
	}

	/**
	 * @param $var
	 * @return helpers\validate
	 */
	public function validate($var){
		return $this->validateHelper->set($var);
	}

	/**
	 * @param $var
	 * @return helpers\data
	 */
	public function data($var){
		return $this->dataHelper->set($var);
	}
}