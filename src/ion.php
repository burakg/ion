<?php
/**
 * Class ion
 *
 * Main wrapper that fabricates sub wrapper classes
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * Created on: 12.12.2014
 *
 */

namespace burakg\ion;

use burakg\ion\dbBase\dbConn;

final class ion
{
    use singletonTrait;

    protected
        $vars,
        $settings,
        $apps,
        $db,
        $front,
        $language = null,
        $helpers;

    protected function __construct()
    {
        $this->apps = [];
        $this->settings = [];
        $this->db = dbConn::get();
        $this->front = front::get();
        $this->helpers = helpers::get();
    }

    /**
     * Returns helper methods
     * @return helpers
     */
    public function helpers(): helpers
    {
        return $this->helpers;
    }

    /**
     * Returns front-end methods
     * @return front
     */
    public function front(): front
    {
        return $this->front;
    }

    /**
     * Returns Database object
     * @return dbConn
     */
    public function db(): dbConn
    {
        return $this->db;
    }

    /**
     * Returns Language object
     * @return language
     */
    public function language(): language
    {
        if ($this->language === null)
            $this->language = language::get();

        return $this->language;
    }

    /**
     * Returns settings object
     * @param $var
     * @return mixed
     */
    public function settings($var)
    {
        return $this->settings[$var];
    }

    /**
     * @param string $app_name
     * @return ion
     */
    public function register_app(string $app_name): ion
    {
        if (!array_key_exists($app_name, $this->apps) && file_exists(CB_APPS . $app_name . '/appConfig.php')) {
            $appSettings = require CB_APPS . $app_name . '/appConfig.php';
            $this->apps[$app_name] = $appSettings;
            $this->front->router()->route($appSettings->CF_ROOT . '(.*)', function () use ($app_name) {
                $this->settings['APP_NAME'] = $app_name;
                ion::get()->start_app($app_name);
            }, ['GET', 'POST', 'DELETE', 'PUT', 'HEAD']);

            $loader = new \Composer\Autoload\ClassLoader();
            $name = preg_replace_callback('(-([a-zA-Z]))', function ($match) {
                return strtoupper($match[1]);
            }, $app_name);
            $loader->addPsr4('ionApp\\' . $name . '\\', CB_APPS . $app_name . '/classes');
            $loader->register();
        }

        return $this;
    }

    /**
     * Register all aps present in the apps directory of the project
     */
    public function register_all_apps(): ion
    {
        $directories = glob(CB_APPS . '*', GLOB_ONLYDIR);
        foreach ($directories as $dir) {
            $this->register_app(substr(strrchr($dir, "/"), 1));
        }

        return $this;
    }

    /**
     * @param string $app_name
     * @return $this
     */
    protected function start_app(string $app_name): ion
    {
        if (array_key_exists($app_name, $this->apps)) {
            $this->front->router()->reset();
            foreach ($this->apps[$app_name]->APP_ROUTING as $path => $pathInfo) {
                foreach ($pathInfo[0] as $method) {
                    $this->front->router()->route($this->apps[$app_name]->CF_ROOT . ltrim($path, '/'), $pathInfo[1], $method);
                }
            }
            $this->front->router()->execute($_SERVER['REQUEST_URI']);
        }

        return $this;
    }

    /**
     * @param string|bool $currentApp
     * @return generic\appSetting
     */
    public function get_current_app(string|bool $currentApp = true): generic\appSetting
    {
        return ($currentApp === true) ? $this->apps[$this->settings['APP_NAME']] : $this->apps[$currentApp];
    }

    /**
     * return array
     */
    public function get_app_list(): array
    {
        return array_keys($this->apps);
    }
}