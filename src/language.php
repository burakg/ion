<?php
/**
 *
 * Class language
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 24.03.2015
 *
 */

namespace burakg\ion;

use burakg\ion\front\sitemap;
use burakg\ion\helpers\template;

class language {
	use singletonTrait;

	public function get_langs($include_default=true){
		$output = [];
		foreach(ion::get()->get_current_app()->get_setting('languages') AS $lang){
			if($include_default === false && $lang->default == 1)
				continue;

			if($lang->is_default == 1 && !isset($this->options->defaultLang))
				$this->options->defaultLang = $lang->url;

			$output[$lang->url] = $lang;
		}
		$this->options->languages = $output;

		return $output;
	}

	public function get_current_lang($override=null){
		if($override === null){
			if(helpers::get()->validate($this->options->curLang)->is_null()){
				$data = $this->options->languages[$this->options->defaultLang];
			}else{
				$data = $this->options->languages[$this->options->curLangIndicatior];
			}
		}else{
			$data = $this->options->languages[$override];
		}
		return $data;
	}

    /**
     * @param bool $include_default
     * @return array
     */
	public function get_short_langs($include_default=true){
		$data = $this->get_langs($include_default);
		return array_keys($data);
	}

	/**
	 * @param string $lang
	 * @return language
	 */
	public function set_current_lang($lang=null){
		$available = $this->get_short_langs(true);
		if(isset(ion::get()->curLang) && ion::get()->curLang !== null && in_array(ion::get()->curLang, $available))
			$this->options->curLang = ion::get()->curLang;

		if($lang !== null && in_array($lang, $available))
			$this->options->curLang = $lang;

		if($this->options->curLang === null){
			$node = $this->options->languages[$this->options->defaultLang];
		}else{
			$node = $this->options->languages[$this->options->curLang];
		}
		$this->options->curLang = $node->url;
		$this->options->curLangIndicatior = $node->url;

		$locale = explode(',',$node->locale);
		setlocale(LC_TIME, $locale[0], $locale[1], $locale[2], $locale[3]);

		return $this;
	}

	public function set_app_language($query_data=[]){
		$ion = ion::get();
		$method = $ion->get_current_app()->MULTI_LINGUAL_METHOD;
		switch($method){
			case 'session':
				$user = front\auth::get()->get_user();
				$selected = (helpers::get()->validate($user['language'])->is_null() === false) ? $user['language'] : null;
				$this->set_current_lang($selected);
                $this->options->urlPrefix = null;

				$ion->curLang = $this->options->curLang;
				break;

			case 'url':
                $this->set_current_lang($query_data);
                $this->options->urlPrefix = $this->get_current_lang()->url.'/';

                $ion->curLang = $this->options->curLang;
				break;

            case 'fixed':
                $languages = json_decode(json_encode($ion->get_current_app()->get_setting('languages')),true);
                $currentLang = $languages[0];

                $this->set_current_lang($currentLang->url);
                $this->options->urlPrefix = null;
                $ion->curLang = $this->options->curLang;

                break;
		}
	}

	public function sitemap_file_suffix(){
        if(helpers::get()->validate($this->options->curLang)->is_null() === false){
			$langList = $this->get_short_langs(true);
			return (in_array($this->get_current_lang()->url,$langList)) ? '_'.$this->options->curLang : null;
		}else{
			return null;
		}
	}

    public function language_nav(){
        $output = null;
        $app = ion::get()->get_current_app();

        $langList = $this->get_langs(true);

        $cNode = sitemap::get()->currentNode;
        $suffix = ($cNode['root'] == true) ? null : 'lingual-redirect/'.$cNode['id'];

        $template = template::get();
        foreach($langList AS $l){
            $class = ($l->short == ion::get()->curLang) ? ' active' : null;

            $output .= $template->apply('sub-menu-repeater',[
                'node-class' => 'language-item'.$class,
                'node-url' => $app->CF_ROOT.$l->url.'/'.$suffix,
                'node-title' => $l->short_title,
                'node-target' => null,
                'node-child' => null
            ]);
        }

        return $output;
    }

    public function get_url_prefix($regex=false,$appOverride=null){
        $app = ($appOverride === null) ? ion::get()->get_current_app() : $appOverride;
        if($app->MULTI_LINGUAL === true && $app->MULTI_LINGUAL_METHOD == 'url'){
            if($regex){
                $langList = [];
                foreach($app->get_setting('languages') AS $language){
                    $langList[] = $language->url;
                }
                return '('.implode('|',$langList).')/';
            }else{
                return $this->options->urlPrefix;
            }
        }else{
            return null;
        }
    }
}