<?php
/**
 * Class front
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 18.03.2015
 */

namespace burakg\ion;

/**
 * Class dbBase
 * @package burakg\ion
 * @property array $config->fields
 */
abstract class dbBase {
	protected
		$properties,
		$settings;

	abstract protected function init();

	abstract public function get_list($suffix=null,$paginate=0,$query=array(),$order=null,$prefix=null,$fieldSelector=null);

	abstract public function search($phrase,$fields,$suffix=null,$paginate=0,$translate=null);

}
