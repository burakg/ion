<?php
/**
 * Singleton Trait
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 10.02.2015
 * Time: 17:04
 */

namespace burakg\ion;

trait singletonTrait {
	protected $options;
	protected static $inst = null;

	protected function __construct(){
		$this->options = new generic\option;
		$this->init();
	}
	protected function __clone() {}

	/**
	 * Returns $this->options->$val
	 * @param $val
	 * @return $this->options->$val
	 */
	public function __get($val) {
		return $this->options->$val;
	}

	/**
	 * Sets $this->options->$key to $val
	 * @param $key
	 * @param $val
	 * @return $this
	 */
	public function __set($key,$val) {
	    if(!is_object($this->options))
	        $this->options = new generic\option;

		$this->options->$key = $val;

		return $this;
	}

	/**
	 * Class initiator
	 * @return $this
	 */
	public static function get(){
		if (static::$inst === null){
			static::$inst = new static();
		}
		return static::$inst;
	}

	/**
	 * A generic callback method to be called upon initiating the class
	 */
	protected function init(){}

	/**
	 * Class initiator
	 * @return $this
	 */
	public function re_init(){
		$this->init();

		return $this;
	}
}