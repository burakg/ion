<?php
/**
 *
 * Class dbSettings
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 12.12.2014
 *
 */

namespace burakg\ion\generic;

class pdoSetting {
	protected $vars;

	public function __construct($server,$user,$password,$db_name){
		$this->vars = new \stdClass;
		$this->vars->server = $server;
		$this->vars->user->$user;
		$this->vars->password->$password;
		$this->vars->database->$db_name;
	}

	public function __get($val){
		return $this->$val;
	}
}