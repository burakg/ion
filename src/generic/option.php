<?php
/**
 *
 * Class option
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 17.02.2015
 *
 */

namespace burakg\ion\generic;

class option {
	private $vars;

	public function __construct() {
		$this->vars = [];
	}

	public function __get($key) {
		return $this->vars[$key];
	}

	public function __set($key,$val) {
		$this->vars[$key] = $val;
	}
}