<?php
/**
 *
 * Class appSetting
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 27.02.2015
 *
 */

namespace burakg\ion\generic;

use burakg\ion\language;

/**
 * @property $CB_ROOT
 * @property $CB_ASSETS
 * @property $CF_ROOT
 * @property $CF_ASSETS
 * @property $CF_ASSET_LOAD
 * @property $MASTERPAGE_ROOT
 * @property $DATA_ROOT
 * @property $MODULES_ROOT
 * @property $SITEMAP_MODE
 * @property $AUTH_MODE
 * @property $AUTH_ROOT
 * @property $APP_ROUTING
 * @property $APP_SETTINGS
 * @property $MULTI_LINGUAL
 * @property $MULTI_LINGUAL_METHOD
 * @property $MULTI_LANG_PREFIX
 */
class appSetting
{
    private $vars;
    private $rootPath;

    public function __construct($rootPath)
    {
        $this->rootPath = $rootPath;
        $this->vars = [
            'CB_ROOT' => '/',
            'CB_ASSETS' => null,
            'CF_ROOT' => '/',
            'CF_ASSETS' => null,
            'CF_ASSET_LOAD' => null,
            'MASTERPAGE_ROOT' => null,
            'DATA_ROOT' => null,
            'MODULES_ROOT' => null,
            'SITEMAP_MODE' => 'default',
            'AUTH_MODE' => 'BY_URL',
            'AUTH_ROOT' => true,
            'APP_ROUTING' => [],
            'APP_SETTINGS' => null,
            'MULTI_LINGUAL' => true,
            'MULTI_LINGUAL_METHOD' => 'session',
            'MULTI_LANG_PREFIX' => null,
        ];
    }

    public function __get($key)
    {
        return $this->vars[$key];
    }

    public function __set($key, $val)
    {
        $this->vars[$key] = $val;
        return $this;
    }

    public function set_cb_root(string|null $val): appSetting
    {
        $suffix = ($val != null) ? '/' . $val : null;
        $this->vars['CB_ROOT'] = $this->rootPath . $suffix . '/';

        return $this;
    }

    public function set_cf_root(string|null $val): appSetting
    {
        $suffix = ($val != null) ? '/' . $val . '/' : '/';
        $this->vars['CF_ROOT'] = $suffix;

        return $this;
    }

    public function set_cb_assets(string $dirname): appSetting
    {
        $this->vars['CB_ASSETS'] = $this->vars['CB_ROOT'] . trim($dirname, '/') . '/';

        return $this;
    }

    public function set_cf_assets(string $dirname): appSetting
    {
        $this->vars['CF_ASSETS'] = CF_APPS . trim($dirname, '/') . '/';

        return $this;
    }

    /**
     * @param array $assetArray
     * @return $this
     */
    public function set_assets_to_load(array $assetArray): appSetting
    {
        $this->vars['ASSET_LOAD'] = $assetArray;

        return $this;
    }

    public function set_masterpage_root(string $dirname): appSetting
    {
        $this->vars['MASTERPAGE_ROOT'] = $this->vars['CB_ROOT'] . $dirname . '/';

        return $this;
    }

    public function set_data_root($dirname): appSetting
    {
        $this->vars['DATA_ROOT'] = $this->vars['CB_ROOT'] . trim($dirname, '/') . '/';

        return $this;
    }

    public function set_modules_root($dirname): appSetting
    {
        $this->vars['CB_MODULES_ROOT'] = $this->vars['CB_ROOT'] . trim($dirname, '/') . '/';
        $this->vars['CF_MODULES_ROOT'] = CF_APPS . str_replace(CB_APPS, '', $this->vars['CB_ROOT']) . trim($dirname, '/') . '/';

        return $this;
    }

    public function set_sitemap_mode($val): appSetting
    {
        if (in_array($val, ['moduleLoader', 'custom', 'default']))
            $this->vars['SITEMAP_MODE'] = $val;
        else
            $this->vars['SITEMAP_MODE'] = 'default';

        return $this;
    }

    public function set_auth_mode($val): appSetting
    {
        $this->vars['AUTH_MODE'] = $val;

        return $this;
    }

    public function set_root_authentication($val): appSetting
    {
        if (is_bool($val))
            $this->vars['AUTH_ROOT'] = $val;
        else
            $this->vars['AUTH_ROOT'] = false;

        return $this;
    }

    public function load_app_settings(): appSetting
    {
        $this->vars['APP_SETTINGS'] = (file_exists($this->vars['DATA_ROOT'] . 'settings.json')) ?
            json_decode(file_get_contents($this->vars['DATA_ROOT'] . 'settings.json')) : json_decode('[]');

        return $this;
    }

    public function set_multi_lingual($enabled = true, $method = 'session'): appSetting
    {
        $this->vars['MULTI_LINGUAL'] = $enabled;
        $this->vars['MULTI_LINGUAL_METHOD'] = $method;
        $this->vars['MULTI_LANG_PREFIX'] = language::get()->get_url_prefix(true, $this);

        return $this;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function get_setting($key)
    {
        return $this->APP_SETTINGS->$key;
    }

    /**
     * @param $newData array|string
     * @return $this
     */
    public function update_settings($newData): self
    {
        $settingFile = $this->vars['DATA_ROOT'] . 'settings.json';
        $finalize = (is_array($newData)) ? json_encode($newData, JSON_PRETTY_PRINT) : $newData;
        file_put_contents($settingFile, $finalize);

        return $this;
    }

    /**
     * @param array $method
     * @param string $uri
     * @param callable $callback
     * @param bool $includeLanguagePrefix
     * @return $this
     */
    public function create_route(array $method, string $uri, callable $callback, bool $includeLanguagePrefix = false): self
    {
        $routes = $this->vars['APP_ROUTING'];
        $routes[($includeLanguagePrefix) ? $this->vars['MULTI_LANG_PREFIX'] . $uri : $uri] = [
            $method,
            $callback
        ];
        $this->vars['APP_ROUTING'] = $routes;

        return $this;
    }
}