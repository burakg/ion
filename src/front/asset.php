<?php
/**
 *
 * Class asset
 * You can enqueue CSS and JS files and output them in a single file or as a seperate file
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 11.02.2015
 *
 */

namespace burakg\ion\front;
use burakg\ion AS ion;
use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;

class asset {
    use ion\singletonTrait;
    protected $assets,$template,$output,$cb_path,$cf_path;

    protected function init(){
        $this->cb_path = ion\ion::get()->get_current_app()->CB_ASSETS;
        $this->cf_path = ion\ion::get()->get_current_app()->CF_ASSETS;
        $this->template = ion\helpers\template::get();
        $this->set_assets();
    }

    public function set_assets(){
        $this->assets = ion\ion::get()->get_current_app()->ASSET_LOAD;
    }

    /**
     * Enqueues JS files into arrays and output them both in single joined files or separate files
     * @param $file
     * @param bool $compress
     * @return asset
     */
    public function add_js($file,$compress=false){
        if(array_search($file,$this->assets['js']) === false)
            $this->assets['js'][] = ($compress === true) ? [$file,$compress] : $file;

        return $this;
    }

    /**
     * Merges local JS files into one given file
     * @param string $path
     * @return asset
     */
    protected function merge_js($path){
        $files_to_merge = [];
        $hash_data = [];

        for($i = 0;$i < count($this->assets['js']);$i++){
            if(is_array($this->assets['js'][$i]) && $this->assets['js'][$i][1] === true && strpos($this->assets['js'][$i][0],$this->cb_path) !== false && file_exists($this->assets['js'][$i][0])){
                $files_to_merge[] = $this->assets['js'][$i][0];

                $hash_data[] = ['filename' => $this->assets['js'][$i][0], 'date' => filemtime($this->assets['js'][$i][0])];
                $this->assets['js'][$i] = $this->assets['js'][$i][0];
            }elseif(is_array($this->assets['js'][$i]) && $this->assets['js'][$i][1] !== true && strpos($this->assets['js'][$i][0],$this->cb_path) !== false && file_exists($this->assets['js'][$i][0])){
                $files_to_merge[] = $this->assets['js'][$i][0];
                echo $this->assets['js'][$i][0].' - error<br>';
                $hash_data[] = ['filename' => $this->assets['js'][$i][0], 'date' => filemtime($this->assets['js'][$i][0])];
                $this->assets['js'][$i] = $this->assets['js'][$i][0];
            }elseif(is_array($this->assets['js'][$i]) && $this->assets['js'][$i][1] !== true && strpos($this->assets['js'][$i][0],$this->cb_path) !== true){
                $this->assets['js'][$i] = $this->assets['js'][$i][0];
            }elseif(is_array($this->assets['js'][$i]) && strpos($this->assets['js'][$i][0],$this->cb_path) !== false && !file_exists($this->assets['js'][$i][0])){
                unset($this->assets['js'][$i]);
            }
        }

        $this->assets['js'] = array_diff($this->assets['js'],$files_to_merge);

        $remerge = (!file_exists($path)) ? true : false;
        $hash_info = $this->hash_file($path,json_encode($hash_data));

        if($hash_info['status'] != 'no-change'){
            $remerge = true;
        }

        if($remerge === true){
            $minifier = new JS();

            foreach($files_to_merge AS $file){
                $minifier->add($file);
            }

            $result = $minifier->minify($path);
        }
        else
            $result = $path;

        if($result !== false)
            $this->assets['js'][] = $path;

        return $this;
    }

    /**
     * Enqueues CSS files into arrays and output them both in single joined files or separate files
     * @param $file
     * @param string $media
     * @return asset
     */
    public function add_css($file,$media='screen'){
        if(array_search($file,$this->assets['css'][$media]) === false)
            $this->assets['css'][$media][] = $file;

        return $this;
    }

    /**
     * Merges local CSS files into one given file
     * @param string $path
     * @param string $media
     * @return asset
     */
    protected function merge_css($path,$media='screen'){
        $files_to_merge = [];
        $hash_data = [];

        for($i = 0;$i < count($this->assets['css'][$media]);$i++){
            if(strpos($this->assets['css'][$media][$i],$this->cb_path) !== false && file_exists($this->assets['css'][$media][$i])){
                $files_to_merge[] = $this->assets['css'][$media][$i];

                $hash_data[] = ['filename' => $this->assets['css'][$media][$i], 'date' => filemtime($this->assets['css'][$media][$i])];
            }
        }

        $this->assets['css'][$media] = array_diff($this->assets['css'][$media],$files_to_merge);

        $remerge = (!file_exists($path)) ? true : false;
        $hash_info = $this->hash_file($path,json_encode($hash_data));

        if($hash_info['status'] != 'no-change'){
            $remerge = true;
        }

        if($remerge === true) {
            $minifier = new CSS();

            foreach($files_to_merge AS $file){
                $minifier->add($file);
            }

            $result = $minifier->minify($path);
        }
        else $result = $path;

        if($result !== false)
            $this->assets['css'][$media][] = $path;

        return $this;
    }

    /**
     * Compares new hash data to old one (if exists) then returns the results
     * with or without creating a hash file
     * @param $original
     * @param $new_data
     * @return array
     */
    protected function hash_file($original,$new_data){
        $hash_file = $original.'.hash';
        $hash_data = (file_exists($hash_file)) ? file_get_contents($hash_file) : null;

        if($new_data != $hash_data){
            file_put_contents($hash_file,$new_data);

            return ['status' => 'new', 'date' => filemtime($hash_file)];
        }else{
            return ['status' => 'no-change', 'date' => filemtime($hash_file)];
        }

    }

    /**
     *
     * @param bool|string $merge_js
     * @param bool|string $merge_css
     * @param bool|string $merge_css_print
     * @return array
     */
    public function output($merge_js=false,$merge_css=false,$merge_css_print=false){
        $output = [
            'js' => '',
            'css' => '',
        ];

        $template = ion\helpers\template::get();

        if($merge_js !== false){
            $this->merge_js($merge_js);
        }
        foreach($this->assets['js'] AS $asset){
            if(strpos($asset,$this->cb_path) !== false){
                $tStamp = filemtime($asset);
                $suffix = '?v='.$tStamp;
            }else{
                $suffix = null;
            }
            $output['js'] .= $template->apply('js-link',['asset-path' => str_replace($this->cb_path,$this->cf_path,$asset).$suffix]);
        }

        if($merge_css !== false){
            $this->merge_css($merge_css,'screen');
        }
        foreach($this->assets['css']['screen'] AS $asset){
            if(strpos($asset,$this->cb_path) !== false){
                $tStamp = filemtime($asset);
                $suffix = '?v='.$tStamp;
            }else{
                $suffix = null;
            }
            $output['css_screen'] .= $template->apply('css-link',['asset-media' => 'screen', 'asset-path' => str_replace($this->cb_path,$this->cf_path,$asset).$suffix]);
        }

        if($merge_css_print !== false){
            $this->merge_css($merge_css_print,'print');
        }
        foreach($this->assets['css']['print'] AS $asset){
            if(strpos($asset,$this->cb_path) !== false){
                $tStamp = filemtime($asset);
                $suffix = '?v='.$tStamp;
            }else{
                $suffix = null;
            }
            $output['css_print'] .= $template->apply('css-link',['asset-media' => 'print', 'asset-path' => str_replace($this->cb_path,$this->cf_path,$asset).$suffix]);
        }

        return $output;
    }

    /**
     * @return asset
     */
    public function reset(){
        $this->assets = [
            'css' => [
                'screen' => [],
                'print' => []
            ],
            'js' => []
        ];

        return $this;
    }
}