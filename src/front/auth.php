<?php
/**
 *
 * Class auth
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 12.03.2015
 *
 */

namespace burakg\ion\front;
use burakg\ion AS ion;

class auth {
	use ion\singletonTrait;

	/**
	 * @return bool
	 */
	protected function is_session_started(){
		if(php_sapi_name() !== 'cli'){
			if(version_compare(phpversion(), '5.4.0', '>=')){
				return session_status() === PHP_SESSION_ACTIVE ? true : false;
			}else{
				return session_id() === '' ? false : true;
			}
		}
		return false;
	}

	/**
	 * @return auth
	 */
	protected function init(){
		session_name(ion\ion::get()->settings('APP_NAME'));

		if($this->is_session_started() === false){
			session_start();
		}
		if(!isset($_SESSION['ionUser'])){
			$_SESSION['ionUser'] = [
				'lastActive' => 0
			];
		}

		if($_SESSION['ionUser']['lastActive'] + ion\ion::get()->get_current_app()->APP_SETTINGS->sitemap->sessionTimeout < time()){
			#$this->unset_user();
			$_SESSION['ionUser']['lastActive'] = time();
		}else{
			$_SESSION['ionUser']['lastActive'] = time();
		}

		return $this;
	}

	/**
	 * @param $userData
	 * @return auth
	 */
	public function set_user($userData){
		if(ion\helpers::get()->validate($userData)->is_array() || is_object($userData)){
			$userData = (array)$userData;
			unset($userData['password']);
			foreach((array)$userData AS $key => $value){
				$_SESSION['ionUser'][$key] = $value;
			}
			$_SESSION['ionUser']['login'] = true;
		}
		return $this;
	}

	/**
	 * @return auth
	 */
	public function unset_user(){
		unset($_SESSION['ionUser']);

		return $this;
	}

	/**
	 * @return mixed|bool
	 */
	public function get_user(){
		return (isset($_SESSION['ionUser'])) ? $_SESSION['ionUser'] : false;
	}
}