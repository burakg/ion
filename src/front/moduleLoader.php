<?php
namespace burakg\ion\front;

use burakg\ion AS ion;

class moduleLoader {
    use ion\singletonTrait;
	/**
	 * @var ion\generic\appSetting
	 */
	protected $app;
	protected $dataPath;
	protected $modulePath;
	protected $modulesObjects = [];
	protected $modulesArray = [];
	protected $sitemap = [];
	protected $manifest;
	protected $hierarchy = [];
	protected $translation = [];
    /**
     * @var ion\helpers\phraser $phraser
     */
    protected $phraser;

    protected function init(){
        $app = ion\ion::get()->get_current_app();
        $this->dataPath = $app->DATA_ROOT;
        $this->modulePath = $app->CB_ROOT.'modules/';
        $this->sitemap = json_decode('[]');
        $this->manifest = $this->get_manifest();
    }

    public function get_modules(){
        return $this->manifest['modules'];
    }

    protected function get_manifest(){
		if(file_exists($this->dataPath.'modules.json')){
			$data = json_decode(file_get_contents($this->dataPath.'modules.json'),true);
			if(is_numeric($data['manifest_date']) && $data['manifest_date'] > 0)
				return $data;
		}
;
		$modules = $this->register_modules()->modulesObjects;
		$module_hierarchy = $this->auto_hierarchy();

		$manifest = [
			'modules' => $modules,
			'hierarchy' => [
				'mode' => 'auto',
				'modules' => $module_hierarchy
			],
			'manifest_date' => time()
		];
		file_put_contents($this->dataPath.'modules.json',json_encode($manifest,JSON_PRETTY_PRINT));
        $this->get_sitemap();
		return json_decode(file_get_contents($this->dataPath.'modules.json'),true);
	}

    protected function register_modules(){
		$modules = [];
		$translation_data = json_decode(file_get_contents($this->dataPath.'generic-translations.json'),true);
		foreach(glob($this->modulePath.'*', GLOB_ONLYDIR) as $dir) {
			$module = new module(basename($dir));
			$translation_data = array_merge($translation_data,$module->getTranslation());
			$modules[$module->getManifestArray()['namespace']] = $module;
		}
		file_put_contents($this->dataPath.'translations.json',json_encode($translation_data,JSON_PRETTY_PRINT));
		ion\helpers\phraser::get()->re_init();

		$this->modulesObjects = $modules;

		return $this;
	}

	protected function auto_hierarchy($pending=[]){
		$hierarchy = $this->hierarchy;

		$modules = (is_array($pending) && count($pending) > 0) ? $pending : $this->modulesObjects;

		foreach($modules AS $module){
			$manifest = $module->getManifestArray();
			$parent = $manifest['parentNamespace'];

			if(isset($parent) && !is_null($parent) && strlen($parent) > 0){
				if(array_key_exists($parent,$this->hierarchy)){
					$hierarchy[$parent]->addChildModule($module);
					unset($pending[$manifest['name']]);
				}elseif(!array_key_exists($manifest['name'],$pending) && array_key_exists($manifest['parentNamespace'],$this->modulesObjects)){
					$pending[$manifest['name']] = $module;
				}
			}else{
				$hierarchy[$manifest['name']] = $module;
			}
		}
		$this->hierarchy = $hierarchy;

		if(
			count($pending) > 0 &&
			($this->manifest['hierarchy']['mode'] == 'auto' || is_null($this->manifest['hierarchy']['mode']))
		){
			$this->auto_hierarchy($pending);
		}
		$this->hierarchy = $hierarchy;
        $this->manifest = json_decode(file_get_contents($this->dataPath.'modules.json'),true);

		return $hierarchy;
	}

	protected function register_sitemap($parent,$lang){
		$sitemap = [];

		$this->phraser = ion\helpers\phraser::get();
        $this->phraser->set_language_permanent($lang);
		foreach($parent AS $moduleName => $moduleNode){
			$manifest = $moduleNode->getManifestArray();
            if($manifest['active'] != "true")
                continue;

			$nodeData = $this->parse_nodes($manifest['node'],
				['name' => $manifest['name'],'path' => $moduleNode->getPath()],
				($manifest['parentNamespace'] != null && strlen($manifest['parentNamespace']) ? $manifest['parentNamespace'] : null)
			);

            $subModules = $moduleNode->getChildModules();
			if(count($subModules) > 0){
				$subModules = $this->register_sitemap($subModules,$lang);
                $nodeData[0]['node'] = array_merge((array)$nodeData[0]['node'],$subModules);
            }

			$sitemap[] = $nodeData[0];
		}
		return $sitemap;
	}

    protected function parse_nodes($nodes, $moduleInfo, $parentPath=null,$parentId=0){
        $output = [];
        foreach($nodes AS $moduleNode){
            $n = [
                'id' => substr(md5($moduleInfo['name'].'-'.$moduleNode['title']), 0, 12),
                'title' => $this->phraser->translate($moduleNode['title']),
                'description' => $this->phraser->translate($moduleNode['description']),
                'url' => $moduleNode['url'],
                'fullpath' => ltrim($parentPath.'/'.$moduleNode['url'],'/'),
				'view' => $moduleNode['view'],
				'hiddenfrom' => $moduleNode['hiddenfrom'],
				'customclass' => $moduleNode['customclass'],
				'protected' => $moduleNode['protected'],
				'master' => $moduleNode['master'],
				'type' => $moduleNode['type'],
				'modulepath' => $moduleInfo['path'],
                'parentid' => $parentId
            ];
            if(is_array($moduleNode['node']) && count($moduleNode['node']) > 0) {
                $n['node'] = $this->parse_nodes($moduleNode['node'],$moduleInfo,$n['fullpath'],$n['id']);
            }
            $output[] = $n;
        }
        return $output;
    }

	protected function check_sitemap(){
		$language = ion\language::get();
		$langList = $language->get_short_langs();
		foreach($langList AS $lang){
			$sitemapFile = ion\ion::get()->get_current_app()->DATA_ROOT.'sitemap_'.$lang.'.json';
			$sitemapFileExists = file_exists($sitemapFile);
			$sitemapFileDate = ($sitemapFileExists === true) ? filemtime($sitemapFile) : 0;

			if($sitemapFileDate < $this->manifest['manifest_date']){
				$sitemapData = ['node' => $this->register_sitemap($this->hierarchy,$lang)];
				file_put_contents($this->dataPath.'sitemap_'.$lang.'.json',json_encode($sitemapData,JSON_PRETTY_PRINT));
			}
		}
		return $this;
	}

	public function get_sitemap(){
		$this->check_sitemap();

		$sitemapSuffix = ion\language::get()->sitemap_file_suffix();
		$sitemapFile = ion\ion::get()->get_current_app()->DATA_ROOT.'sitemap'.$sitemapSuffix.'.json';
		return file_get_contents($sitemapFile);
	}
}