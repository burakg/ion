<?php
/**
 *
 * Class metaTag
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 09.03.2015
 *
 */

namespace burakg\ion\front;

use \burakg\ion AS ion;
use \burakg\ion\helpers AS helpers;

class metaTag {
	protected $tags = [];
	use ion\singletonTrait;

	/**
	 * @param $name
	 * @param $content
	 * @return metaTag
	 */
	public function add_meta($name,$content){
		if(!array_key_exists($name,$this->tags)){
			$this->tags[$name] = ion\helpers\template::get()->apply(
				'meta-tag',
				[
					'name' => $name,
					'content' => $content
				]
			);
		}

		return $this;
	}

	/**
	 * @param $name
	 * @param $content
	 * @return metaTag
	 */
	public function add_opengraph($name,$content){
		if(!array_key_exists($name,$this->tags)){
			$this->tags[$name] = ion\helpers\template::get()->apply(
				'opengraph-tag',
				[
					'name' => $name,
					'content' => $content
				]
			);
		}

		return $this;
	}

	/**
	 * @param $input
	 * @return metaTag
	 */
	public function add_custom($name,$input){
		if(!array_key_exists($name,$this->tags) && is_string($input) && preg_match('/<[link|style|meta]\s.*?>/', $input) !== false){
			$this->tags[$name] = $input;
		}

		return $this;
	}

	/**
	 * @return metaTag
	 */
	public function reset(){
		$this->tags = [];

		return $this;
	}

	/**
	 * @return string|null
	 */
	public function output(){
		return implode(null,$this->tags);
	}
}