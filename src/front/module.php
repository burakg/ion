<?php
/**
 * User: Burak Mehmet Gürbüz
 * Date: 11.06.2016
 * Time: 00:47
 */

namespace burakg\ion\front;

use burakg\ion AS ion;

class module
{
    /**
     * @var string $name
     */
    public $name;
    /**
     * @var $author
     */
    public $author;
    /**
     * @var $version
     */
    public $version;
    /**
     * @var string $dataPath
     */
    protected $dataPath;
    /**
     * @var string $moduleRoot
     */
    public $moduleRoot;
    /**
     * @var array $manifest
     */
    public $manifest;
    protected $parentData=null;
    protected $cbRoot;

    public $modules=[];

    public function __construct($moduleName)
    {
        $app = ion\ion::get()->get_current_app();
        $this->cbRoot = $app->CB_ROOT;
        $this->dataPath = str_replace($this->cbRoot,null,$app->DATA_ROOT);
        $this->moduleRoot = str_replace($this->cbRoot,null,$app->CB_MODULES_ROOT);
        $this->modulePath = $this->moduleRoot.$moduleName.'/';
        $this->name = $moduleName;

        $this->getManifest();

        $this->title = $this->manifest['title'];
        $this->version = $this->manifest['version'];
        $this->author = $this->manifest['author'];
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->$name;
    }

    public function __toString()
    {
        return $this->getManifestJSON();
    }

    /**
     * @param module $parentData
     * @return $this
     */
    public function setParent(module $parentData)
    {
        $this->parentData = $parentData;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        $parentPath = $this->getParentPath();
        if(!is_null($parentPath)) $parentPath = $parentPath.'/';
        return $parentPath.$this->getManifestArray()['namespace'];
    }

    protected function getParentPath()
    {
        if($this->parentData !== null && method_exists($this->parentData,'getPath')){
            $parentPath = $this->parentData->getPath();
            return $parentPath;
        }
        return null;
    }

    public function addChildModule(module $module)
    {
        $children = $this->modules;
        $children[$module->getManifestArray()['name']] = $module;
        $this->modules = $children;

        return $this;
    }

    public function getChildModules()
    {
        return $this->modules;
    }

    /**
     * @return $this
     */
    protected function getManifest()
    {
        $this->manifest = (file_exists($this->cbRoot.$this->modulePath.'data/manifest.json')) ?
            json_decode(file_get_contents($this->cbRoot.$this->modulePath.'data/manifest.json'),true) : [];

        return $this;
    }

    /**
     * @return string
     */
    public function getManifestJSON()
    {
        return json_encode($this->manifest,JSON_PRETTY_PRINT);
    }

    /**
     * @return array
     */
    public function getManifestArray()
    {
        return $this->manifest;
    }

    /**
     * @return object
     */
    public function getManifestJSONobject()
    {
        return json_decode(json_encode($this->manifest));
    }

    public function getTranslation()
    {
        return (file_exists($this->cbRoot.$this->modulePath.'data/translations.json')) ?
            json_decode(file_get_contents($this->cbRoot.$this->modulePath.'data/translations.json'),true) : [];
    }

    protected function writeManifest()
    {
        $file = $this->cbRoot.$this->modulePath.'data/manifest.json';
        $data = json_encode($this->manifest,JSON_PRETTY_PRINT);
        return file_put_contents($file,$data);
    }

    public function changeStatus()
    {
        if($this->manifest['forceActive'] == "true")
            return ion\helpers::get()->data(['status' => 0, 'data' => ion\helpers\phraser::get()->translate('CANNOT_COMPLY')])->ajax_response();

        if($this->manifest['active'] == "true")
            $this->manifest['active'] = "false";
        else
            $this->manifest['active'] = "true";

        $writeOut = $this->writeManifest();
        if($writeOut !== false)
            return ion\helpers::get()->data(['status' => 1, 'data' =>  ($this->manifest['active'] == "true") ?
                    ion\helpers\phraser::get()->translate('MODULE_ACTIVATED') :
                    ion\helpers\phraser::get()->translate('MODULE_DEACTIVATED')
            ])->ajax_response();
        else
            return ion\helpers::get()->data(['status' => 1, 'data' => ['message' => '']])->ajax_response();
    }
}