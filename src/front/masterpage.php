<?php
/**
 *
 * Class masterpage
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 10.02.2015
 *
 */

namespace burakg\ion\front;

use \burakg\ion\ion;
use \burakg\ion\helpers AS helpers;
use burakg\ion\singletonTrait;

class masterpage {
	use singletonTrait;
	protected
		$settings,
		$templateFields;

	protected function init(){
		$asset = asset::get();
		$path = ion::get()->get_current_app()->CB_ASSETS;
		$assetOutput = $asset->output($path.'js/merged.js',$path.'css/merged_screen.css',$path.'css/merged_print.css');
		$asset->reset();

		$this->templateFields = [
			'title' => function(){
				return $this->set_node_title();
			},
			'assets' => ion::get()->get_current_app()->CF_ASSETS,
			'metatags' => function(){
				return $this->set_meta_tags();
			},
			'stylesheets' => function() use ($assetOutput){
				return $assetOutput['css_screen'].$assetOutput['css_print'];
			},
			'scripts' => function() use ($assetOutput){
				return $assetOutput['js'];
			}
		];
		$this->options->delimiters = ion::get()->get_current_app()->get_setting('delimiters') ?? ['{{','}}'];
	}

	/**
	 * @param callable $callback
	 * @return masterpage
	 */
	public function set_callback($callback){
		if(is_callable($callback))
			$this->settings['callback'] = $callback;
		return $this;
	}

	/**
	 * Set template fields for the masterpage in order to exchange the placeholders on masterpage file
	 * @param mixed|string $templateData
	 * @param null $callback
	 * @return masterpage
	 */
	public function set_template_fields($templateData,$callback=null){
		if(is_array($templateData) && is_null($callback)){
			$this->templateFields[$templateData[0]] = $templateData[1];
		}elseif(is_string($templateData) && $callback !== null){
			$this->templateFields[$templateData] = $callback;
		}

		return $this;
	}

	/**
	 * @param mixed|string $templateField
	 * @param $value
	 * @return masterpage
	 */
	public function apply_template_field($templateField,$value){
		$master_text = $this->settings['masterpage'];

		$exists = strpos($master_text,$templateField);

		if($exists !== false){
			$exchange = (is_callable($value)) ? call_user_func($value) : $value;

			$master_text = str_replace(
			    $this->options->delimiters[0].$templateField.$this->options->delimiters[1],
                $exchange,
                $master_text
            );
		}
		$this->settings['masterpage'] = $master_text;

		return $this;
	}

	/**
	 * @param bool|null|string $overRide
	 * @return masterpage
	 */
	public function load_masterpage($overRide=false){
		$masterpage_root = ion::get()->get_current_app()->MASTERPAGE_ROOT;

		if($overRide === null || $overRide === false){
			$masterpage_file =
				(
					helpers::get()->validate(sitemap::get()->get_current_node()['master'])->is_null() === true
					||
					!file_exists($masterpage_root.sitemap::get()->get_current_node()['master'])
				) ? $masterpage_root.'default.php' : $masterpage_root.sitemap::get()->get_current_node()['master'];
		}else{
			$masterpage_file = $masterpage_root.$overRide;
		}
		$this->settings['masterpage'] = file_get_contents($masterpage_file);

		return $this;
	}

	/**
	 * Ignites the masterpage engine and starts filling the placeholders
	 * @param bool $seekView
	 * @param bool $overRideMasterpage
	 * @return masterpage
	 */
	public function ignite($seekView=false, $overRideMasterpage=false){
		switch(sitemap::get()->get_current_node()['httpResponse']){
			case 200:
				$this->load_masterpage($overRideMasterpage);
				break;
			case 401:
			case 403:
			case 404:
				$this->load_masterpage('error.php');
				break;
		}

        if($seekView===true){
            $view = sitemap::get()->seek_view();
            $content = $this->run_page($view);
            $this->apply_template_field('content',$content);
        }

		$this->template();

		return $this;
	}

	/**
	 * Replaces the template fields one by one
	 * @return masterpage
	 */
	protected function template(){
		if(is_callable($this->settings['callback'])){
			call_user_func($this->settings['callback']);
		}

		foreach($this->templateFields AS $field => $value){
            $this->apply_template_field($field,$value);
        }

		/** Clean up the empty template fields */
		$final = preg_replace(
		    "/{$this->options->delimiters[0]}\s*[\w\.]+\s*{$this->options->delimiters[1]}/",
            null,
            $this->settings['masterpage']
        );
		$this->settings['masterpage'] = $final;

		return $this;
	}

    /**
     * @param null|string $override
     * @return string
     */
	public function set_node_title($override=null){
		$titles = [];
		$sm = sitemap::get();
		if(isset($sm->get_current_node()['title']))
			$titles[] = $sm->get_current_node()['title'];
		$titles[] = ($override === null) ? ion::get()->get_current_app()->APP_SETTINGS->siteInfo->title : $override;
		return implode(' | ',$titles);
	}

	protected function set_meta_tags(){
		$tagManager = metatag::get();
		$sm = sitemap::get();
		$tagManager->add_meta("description",$sm->get_current_node()['description']);
		$tagManager->add_meta("keywords",$sm->get_current_node()['keywords']);

		return $tagManager->output();
	}

    /**
     * @param $filePath
     * @param null $data
     * @return string|int
     */
	public function run_page($filePath, $data=null){
		if(file_exists($filePath)){
			ob_start();

			include($filePath);

			$content = ob_get_clean();
		}else{
			$content = 404;
		}
		return $content;
	}

	/**
	 * @return string
	 */
	public function end(){
		if(sitemap::get()->get_current_node()['httpResponse'] == 404) {
			header("HTTP/1.0 404 Not Found");
		}
		return $this->settings['masterpage'];
	}
}
