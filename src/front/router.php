<?php
/**
 *
 * Class router
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 16.02.2015
 *
 */

namespace burakg\ion\front;
use burakg\ion;

class router {
	use ion\singletonTrait;
	protected $patterns;

	protected $methods = ['GET','POST','PUT','PATCH','DELETE','HEAD'];

	protected function init(){
		$this->reset();
	}

	/**
	 * @param null $appOverride
	 * @return null|string
	 */
	public function language_prefix($appOverride=null){
		$app = ($appOverride === null) ? ion\ion::get()->get_current_app() : $appOverride;
		if($app->MULTI_LINGUAL === true && $app->MULTI_LINGUAL_METHOD == 'url'){
			$langList = [];
			foreach($app->get_setting('languages') AS $language){
				$langList[] = $language->short;
			}
			return '('.implode('|',$langList).')/';
		}else{
			return null;
		}
	}

	/**
	 * @param string $pattern
	 * @param callable $callback
	 * @param string|array $method
	 * @return router
	 */
	public function route($pattern, $callback, $method = 'GET'){
		if(
			(
				(
					is_string($method) &&
					in_array($method,$this->methods)
				)
				||
				(
					is_array($method) &&
					count(array_intersect($method,$this->methods)) > 0
				)
			)
		){
			$pattern = '/^'.str_replace('/', '\/', $pattern).'$/';

			if(!is_array($method)){
				$this->patterns[$pattern][$method] = $callback;
			}else{
				foreach($method AS $methodName){
					$this->patterns[$pattern][$methodName] = $callback;
				}
			}
		}

		return $this;
	}

	/**
	 * @param string $url
	 * @return null|mixed
	 */
	public function execute($url) {
		$requestMethod = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';

		foreach ($this->patterns as $pattern => $callbackMethods) {
			$preg = preg_match($pattern, $url, $params);
			if ($preg) {
				array_shift($params);
				return call_user_func_array($callbackMethods[$requestMethod], array_values($params));
			}
		}
		return null;
	}

	/**
	 * Reset the routing paths to start from scratch
	 * @return router
	 */
	public function reset(){
		$this->patterns = [];
		return $this;
	}
}