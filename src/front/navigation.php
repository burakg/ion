<?php
/**
 *
 * Class navigation
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 09.02.2015
 *
 */

namespace burakg\ion\front;
use burakg\ion as ion;

/**
 * Class navigation
 * @package burakg\ion\front
 * @property ion\helpers\template $template
 */
class navigation {
	use ion\singletonTrait;
	protected $template;

	protected function init(){
		$this->template = ion\helpers\template::get();
	}

    /**
     * Returns single level navigation from sitemap object root
     * @param null $hiddenFrom
     * @param null $p_node
     * @return null|string
     * @throws \Exception
     */
	public function nav_single_level($hiddenFrom=null,$p_node=null){
		$nav = null;

		$nodes = ($p_node !== null && is_array($p_node->node)) ? $p_node->node : sitemap::get()->sitemap->node;
		if(is_array($nodes)){
			foreach($nodes AS $node){
				if(sitemap::get()->check_permissions($node) != 200)
					continue;

				$filter = (ion\helpers::get()->validate($node->hiddenfrom)->is_null() === true) ? [] : explode(',',$node->hiddenfrom);
				if(in_array($hiddenFrom,$filter))
					continue;

				$tempFields = [
					'node-class' => $this->node_class($node),
					'node-url' => $this->node_url($node),
					'node-title' => $node->title,
					'node-child' => null,
					'node-target' => (ion\helpers::get()->validate($node->target)->is_null() !== true) ? ' target="'.$node->target.'"' : null
				];

				$nav .= $this->template->apply("menu-repeater",$tempFields);
			}
		}
		return $nav;
	}

    /**
     * Returns nested navigation menu from sitemap object (json object)
     * @param null $hiddenFrom
     * @param null $p_node
     * @return null|string
     * @throws \Exception
     */
	public function nav_nested($hiddenFrom=null,$p_node=null){
		$nav = null;

		$nodes = ($p_node !== null && is_array($p_node->node)) ? $p_node->node : sitemap::get()->sitemap->node;

		if(is_array($nodes)){
			foreach($nodes AS $c_node){
				if(sitemap::get()->check_permissions($c_node) != 200)
					continue;

				$filter = (ion\helpers::get()->validate($c_node->hiddenfrom)->is_null() === true) ? [] : explode(',',$c_node->hiddenfrom);
				if(in_array($hiddenFrom,$filter))
					continue;

				$tempFields = [
					'node-class' => $this->node_class($c_node),
					'node-url' => $this->node_url($c_node),
					'node-title' => $c_node->title,
					'node-target' => (ion\helpers::get()->validate($c_node->target)->is_null() !== true) ? ' target="'.$c_node->target.'"' : null
				];

				$child = (ion\helpers::get()->validate($c_node->node)->is_array() === true) ? $this->nav_nested($hiddenFrom,$c_node) : null;
				$tempFields['node-child'] = (ion\helpers::get()->validate($child)->is_null() !== true) ? $this->template->apply("sub-menu-wrapper",["submenu" => $child]) : null;

				$nav .= $this->template->apply("menu-repeater",$tempFields);
			}
		}
		return $nav;
	}

    /**
     * Returns breadcrumb navigation from current sitemap node tree
     * @param bool $showMainPage
     * @return mixed|null|string
     * @throws \Exception
     */
	public function nav_breadcrumbs($showMainPage = true){
		$tree = sitemap::get()->nodeTree;

        $bc = null;

		if($showMainPage){
            $tempFields = [
                'node-url' => ion\ion::get()->get_current_app()->CF_ROOT,
                'node-title' => ion\helpers\phraser::get()->translate('MAINPAGE')
            ];
            $bc = $this->template->apply("breadcrumb-home",$tempFields);
        }
		$i=0;
		$p_node = null;
		foreach($tree AS $node){
			$i++;
			$tempFields = [
				'node-url' => $this->node_url($node),
				'node-class' => ($i==count($tree)) ? 'bread-current' : '',
				'node-title' => $node['title'],
                'position' => $i
			];
			$bc .= $this->template->apply("breadcrumb-node",$tempFields);
		}
		return $bc;
	}

	/**
	 * Returns the class list for the given node
	 * @param $node
	 * @return null|string
	 */
	public function node_class($node){
		$class = [];
		$class[] = 'node-'.$node->id;
		$class[] = 'node-'.$node->url;

		if(ion\helpers::get()->validate($node->customclass)->is_null() !== true)
			$class[] = $node->customclass;

		if(ion\helpers::get()->validate($node->active)->is_null() !== true && $node->active == "true")
			$class[] = 'active';

		if(ion\helpers::get()->validate($node->node)->is_array() > 0)
			$class[] = 'has-children';

		return implode(' ',$class);
	}

	/**
	 * Returns a URL prefix including previous node URLS and
	 * @param $node
	 * @return string
	 */
	protected function node_prefix($node){
		if(is_object($node)) $node = (array)$node;
		return (ion\helpers::get()->validate($node['url'])->is_null() !== true) ? ion\ion::get()->get_current_app()->CF_ROOT.$node['url'].'/' : ion\ion::get()->get_current_app()->CF_ROOT;
	}

	protected function lang_prefix(){
		$lang = ion\language::get();
		return $lang->get_url_prefix();
	}

	/**
	 * @param array $node
	 * @return string
	 */
	public function node_url($node){
		if(is_object($node)) $node = (array)$node;

		return (ion\helpers::get()->validate($node['staticurl'])->is_null()) ? CF_ROOT.ion\ion::get()->get_current_app()->CF_ROOT.$this->lang_prefix().$node['fullpath'] : $node['staticurl'];
	}

	/**
	 * @param $attr
	 * @param $val
	 * @return string
	 */
	public function node_url_by_attr($attr,$val){
		$node = sitemap::get()->node_by_attr($attr,$val);

		return $this->node_url($node[0]);
	}
}