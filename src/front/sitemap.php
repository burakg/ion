<?php
/**
 *
 * Class sitemap
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 10.02.2015
 *
 */

namespace burakg\ion\front;
use Peekmo\JsonPath\JsonPath;
use Peekmo\JsonPath\JsonStore;
use burakg\ion as ion;

class sitemap {
	use ion\singletonTrait;

	private string $mode;
    private JsonStore $jPath;

    protected function init(){
		$app = ion\ion::get()->get_current_app();
		$this->mode = $app->SITEMAP_MODE;

		switch($this->mode){
			case 'moduleLoader':
				$this->load_modules()->load_sitemap();
				break;
			default:
				$this->load_sitemap_file()->load_sitemap();
		}
	}

	/**
	 * @return sitemap
	 */
	public function load_sitemap(){
		$this->jPath = new JsonStore($this->options->sitemap);

		$root = ion\ion::get()->get_current_app()->CF_ROOT;

		$langPrefix = ion\language::get()->get_url_prefix();

		$curPath = ($root == '/') ? str_replace(CF_ROOT.$root.$langPrefix,$root,CF_URL) : str_replace(CF_ROOT.$root.$langPrefix,$root,CF_URL);
		$exp = explode('?',$curPath);
		$curPath = $exp[0];

		if($curPath == $root){
			$currentNode = [
				'description' => ion\helpers\phraser::get()->translate('SITE_DESCRIPTION'),
				'keywords' => ion\helpers\phraser::get()->translate('SITE_KEYWORDS'),
				'root' => true
			];
			if(ion\ion::get()->get_current_app()->AUTH_ROOT === true)
				$currentNode['httpResponse'] = $this->check_permissions($currentNode);
			else
				$currentNode['httpResponse'] = 200;

			$this->options->currentNode = $currentNode;
		}else{
			$uri = ($root == '/') ? ltrim($curPath,'/') : str_replace($root,null,$curPath);

			$fragments = explode('/',$uri);
			$nodeTree = [];

			$parentId = '';
			foreach($fragments AS $fragment){
				$query = $parentId.".node[?(@.url=='".$fragment."')]";
				$node = $this->jPath->get("$".$query, true);
				if(is_array($node) && count($node) == 1){
					$this->jPath->add("$".$query,"true","active");
					$parentId = $query;

					$node[0]['httpResponse'] = $this->check_permissions($node[0]);
					$node[0]['active'] = "true";

					$nodeTree[] = $node[0];
				}else{
					$nodeTree[] = [
						'title' => 404,
						'httpResponse' => 404
					];
					break;
				}
			}

			$this->options->nodeTree = $nodeTree;
			$this->options->currentNode = end($nodeTree);
			$this->options->sitemap = $this->jPath->toObject();
			$this->jPath = new jsonstore($this->options->sitemap);

			$this->redirect($this->options->currentNode['redirect']);
		}

		return $this;
	}

	public function load_sitemap_file(){
		$sitemapSuffix = ion\language::get()->sitemap_file_suffix();
		$sitemapFile = ion\ion::get()->get_current_app()->DATA_ROOT.'sitemap'.$sitemapSuffix.'.json';
		if(file_exists($sitemapFile)){
			$this->options->sitemap = json_decode(file_get_contents($sitemapFile));
		}else{
			$this->options->sitemap = json_decode('[]');
		}

		return $this;
	}

	public function load_modules(){
		$moduleLoader = moduleLoader::get();
		$this->options->sitemap = json_decode($moduleLoader->get_sitemap());

		return $this;
	}

	/**
	 * @param $query
	 * @return mixed
	 */
	public function select_node($query){
		if($query !== null && is_string($query)){
			return $this->jPath->get($query);
		}
	}

	/**
	 * Query sitemap by filtering nodes with an attribute and a value
	 * @param $attr
	 * @param $val
	 * @return array|bool
	 */
	public function node_by_attr($attr,$val){
		return $this->select_node("$..node[?(@.{$attr}=='{$val}')]");
	}

	/**
	 * @param $node
	 * @param $authOverride
	 * @return int
	 */
	public function check_permissions($node,$authOverride=null){
		if(is_object($node))
			$node = (array)$node;

		$mode = ion\ion::get()->get_current_app()->AUTH_MODE;
		switch($mode){
			case 'BY_URL':
				return $this->check_permissions_by_url($node,$authOverride);
				break;
			default:
				return $this->check_permissions_bitwisely($node,$authOverride);
		}
	}

	public function check_permissions_bitwisely($node,$authOverride=null){
		if(($node['permissions'] == 0 || ion\helpers::get()->validate($node['permissions'])->is_null()) && $node['forcelogin'] !== true){
			return 200;
		}else{
			if(ion\ion::get()->get_current_app()->APP_SETTINGS->sitemap->ionAuth == true){
				$auth = ion\front\auth::get();
				$user = ($authOverride === null) ? $auth->get_user() : $authOverride;
				if($user['login'] !== true || ion\helpers::get()->validate($user['permissions'])->is_null() || $user['permissions'] == 0){
					return 401; # needs login
				}elseif($user['wheel_access'] == 1 || ((int)$user['permissions'] & (int)$node['permissions']) || $node['permissions'] == 0){
					return 200; # access granted
				}else{
					return 403; # user not authorized
				}
			}
		}
		return 401;
	}

	public function check_permissions_by_url($node,$authOverride=null){
		if(ion\ion::get()->get_current_app()->APP_SETTINGS->sitemap->ionAuth == true){
			$user = ($authOverride === null) ? ion\front\auth::get()->get_user() : $authOverride;

			if($user['login'] !== true || count($user['permissions']) == 0)
				return 401; # needs login

			if($user['wheel_access'] == 1 || (in_array($node['fullpath'],$user['permissions']) || ($user['login'] === true && $node['root'] === true))){
				return 200; # access granted
			}else{
				return 403; # user not authorized
			}
		}
		return 401;
	}

    /**
     * @param null|string $forceView
     * @return string
     */
	public function seek_view($forceView=null){
		switch($this->options->currentNode['httpResponse']){
			case 200:
				if($forceView !== null){
					return ion\ion::get()->get_current_app()->CB_ROOT.$forceView;
				}

				if($this->options->currentNode['root'] === true)
					return ion\ion::get()->get_current_app()->CB_ROOT.'views/index.php';

				if($this->mode == 'moduleLoader'){
					$fileName = ion\ion::get()->get_current_app()->CB_ROOT.'modules/'.
						$this->options->currentNode['modulepath'].'/views/'.$this->options->currentNode['view'];

					if(file_exists($fileName)){
						return $fileName;
					}
				}

				$priority = ion\ion::get()->get_current_app()->APP_SETTINGS->sitemap->mvcPriority;
				foreach($priority AS $view_seek){
					$fileName = ion\ion::get()->get_current_app()->CB_ROOT.'views/page-'.$view_seek.'-'.$this->options->currentNode[$view_seek].'.php';
					if(file_exists($fileName)){
						return $fileName;
					}
				}
				break;

			case 401:
				return ion\ion::get()->get_current_app()->CB_ROOT.'views/401.php';
				break;

			case 403:
				return ion\ion::get()->get_current_app()->CB_ROOT.'views/403.php';
				break;
		}

		$this->options->currentNode = [
			'title' => 404,
			'httpResponse' => 404
		];
		return ion\ion::get()->get_current_app()->CB_ROOT.'views/404.php';
	}

	/**
	 * @param string $mode
	 */
	public function redirect($mode){
		switch($mode){
			case 'next': # Next node
				$parent = ($this->options->currentNode['root'] === true || $this->options->currentNode['parentid'] == 0) ? $this->options->sitemap->node : $this->node_by_attr('id',$this->options->currentNode['parentid']);

				$arrived = ($this->options->currentNode['root'] === true) ? true : false;
				foreach($parent AS $node){
					$check = $this->check_permissions($node);
					if($arrived === true && ($check == 200 OR $check == 401)){
						header('Location: '.navigation::get()->node_url($node));
						exit;
					}

					if($node->id == $this->options->currentNode['id'])
						$arrived = true;
				}
				break;

			case 'sub': # First sub node
				if(is_array($this->options->currentNode['node'])){
					foreach($this->options->currentNode['node'] AS $node){
						$check = $this->check_permissions($node);
						if($check == 200){
							header('Location: '.navigation::get()->node_url($node));
							exit;
						}elseif($check == 403){
							continue;
						}elseif($check == 401){
							break;
						}
					}
				}
				break;

			case '403next':
				if($this->check_permissions($this->options->currentNode) == 403){
					$parent = ($this->options->currentNode['root'] === true || $this->options->currentNode['parentid'] == 0) ? $this->options->sitemap->node : $this->node_by_attr('id',$this->options->currentNode['parentid']);

					$arrived = ($this->options->currentNode['root'] === true) ? true : false;
					foreach($parent AS $node){
						$check = $this->check_permissions($node);
						if($arrived === true && ($check == 200 OR $check == 401)){
							header('Location: '.navigation::get()->node_url($node));
							exit;
						}

						if($node->id == $this->options->currentNode['id'])
							$arrived = true;
					}
				}
				break;

			default:
				return;
		}
	}

	/**
	 * @param bool $sameLevel
	 * @param null|array $parentNode
	 * @return array
	 */
	public function get_roles($sameLevel=false,$parentNode=null){
		$output = [];
		if($sameLevel === false){
		    if($parentNode === null)
		        $parentNode = $this->options->currentNode;

			foreach($parentNode['node'] AS $node){
				if($this->check_permissions($node) == 200)
					$output[] = $node;
			}
		}else{
			$parentNode = $this->node_by_attr("id",$this->options->currentNode['parentid'])[0];
			foreach($parentNode['node'] AS $node){
				if($this->check_permissions($node) == 200 && $node['id'] != $this->options->currentNode['id'])
					$output[] = $node;
			}
		}
		return $output;
	}

    /**
     * @param array $nodeData
     */
	public function set_current_node($nodeData){
	    $this->options->currentNode = $nodeData;

	    $tree = $this->options->nodeTree ?? [];
	    $tree[count($tree)-1] = $this->options->currentNode;
	    $this->options->nodeTree = $tree;
    }

    /**
     * @return array
     */
    public function get_current_node(){
	    return $this->options->currentNode;
    }
}