<?php
/**
 *
 * Class front
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 13.12.2014
 *
 */

namespace burakg\ion;

class front {
	protected
		$masterpage,
		$sitemap,
		$router,
		$asset,
		$metaTag,
		$navigation;

	use singletonTrait;

	protected function __construct(){
		$this->router = front\router::get();
	}

	/**
	 * @return front
	 */
	public function start_frontend(){
		$this->sitemap = front\sitemap::get();
		$this->navigation = front\navigation::get();
		$this->masterpage = front\masterpage::get();
		$this->asset = front\asset::get();
		$this->metaTag = front\metaTag::get();

		return $this;
	}

	/**
	 * @return front\masterpage
	 */
	public function masterpage(){
		return $this->masterpage;
	}

	/**
	 * @return front\sitemap
	 */
	public function sitemap(){
		return $this->sitemap;
	}

	/**
	 * @return front\navigation
	 */
	public function navigation(){
		return $this->navigation;
	}

	/**
	 * @return front\router
	 */
	public function router(){
		return $this->router;
	}
}