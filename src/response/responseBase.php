<?php

namespace burakg\ion\response;

class responseBase
{
    protected
        $message,
        $code,
        $status,
        $data;

    public function __construct(
        $message,
        $code,
        $status,
        $data
    ){
        $this->message = $message;
        $this->code = $code;
        $this->status = $status;
        $this->data = $data;
    }

    public function __get($name)
    {
        if(property_exists($this,$name)){
            return $this->$name;
        }else{
            return false;
        }
    }

    public function __set($name, $value)
    {
        if(property_exists($this,$name)) {
            $this->$name = $value;
            return $this;
        }else{
            return false;
        }
    }
}