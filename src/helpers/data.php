<?php
/**
 *
 * Class htmlHelpers
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 11.02.2015
 *
 */

namespace burakg\ion\helpers;
use burakg\ion AS ion;

class data {
	use ion\singletonTrait;
	private $phrase;

	public function set($val){
		$this->phrase = $val;
		return $this;
	}

	public function ajax_response($json=false){
		$data = ['data' => $this->phrase['data'],'status' => $this->phrase['status']];
		return ($json === false) ? $data : json_encode($data,JSON_PRETTY_PRINT);
	}

	public function pre_print(){
		echo '<pre>',print_r($this->phrase,1),'</pre>';
	}

	public function time_elapsed_string(){
		$etime = time() - strtotime($this->phrase);

		if ($etime < 1){
			return '0 seconds';
		}

		$a = array( 365 * 24 * 60 * 60  =>  'year',
			30 * 24 * 60 * 60  =>  'month',
			24 * 60 * 60  =>  'day',
			60 * 60  =>  'hour',
			60  =>  'minute',
			1  =>  'second'
		);
		$a_plural = array( 'year'   => 'years',
			'month'  => 'months',
			'day'    => 'days',
			'hour'   => 'hours',
			'minute' => 'minutes',
			'second' => 'seconds'
		);

		foreach ($a as $secs => $str){
			$d = $etime / $secs;
			if ($d >= 1){
				$r = round($d);
				return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
			}
		}
	}

	public function readable_date($formType=false,$timeFormat=false,$dateFormat='%d %B %Y %H:%M:%S'){
		if(empty($this->phrase) || $this->phrase == '0000-00-00' || $this->phrase == '0000-00-00 00:00:00') return "-";
		$time = ($timeFormat === false) ? strtotime($this->phrase) : $this->phrase;
		return ($formType == false) ? strftime($dateFormat,$time) : strftime($dateFormat === NULL ? '%d.%m.%Y' : $dateFormat,$time);
	}

	/**
	 * Return human readable sizes
	 *
	 * @return string
	 */
	public function readable_size(){
		$bytes = floatval($this->phrase);
		$arBytes = array(
			0 => array(
				"UNIT" => "TB",
				"VALUE" => pow(1024, 4)
			),
			1 => array(
				"UNIT" => "GB",
				"VALUE" => pow(1024, 3)
			),
			2 => array(
				"UNIT" => "MB",
				"VALUE" => pow(1024, 2)
			),
			3 => array(
				"UNIT" => "KB",
				"VALUE" => 1024
			),
			4 => array(
				"UNIT" => "B",
				"VALUE" => 1
			),
		);

		foreach($arBytes as $arItem)
		{
			if($bytes >= $arItem["VALUE"])
			{
				$result = $bytes / $arItem["VALUE"];
				$result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
				break;
			}
		}
		return $result;
	}

	/**
	 * Assigns primary fields of each arrays value to key
	 *
	 * @param string $keyField
	 * @return array
	 */
	public function assign_ids_to_keys($keyField='id'){
		$output = [];
		foreach($this->phrase AS $key => $value)
			$output[$value[$keyField]] = $value;

		return $output;
	}

	public function array_flatten(){
        $return = array();
        array_walk_recursive($this->phrase, function($a) use (&$return) { $return[] = $a; });
        return $return;
    }
}