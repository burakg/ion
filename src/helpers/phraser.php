<?php
/**
 *
 * Class phraser
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 19.03.2015
 *
 */

namespace burakg\ion\helpers;

use Peekmo\JsonPath\jsonstore;
use burakg\ion AS ion;

/**
 * Class phraser
 */
class phraser {
    use ion\singletonTrait;

    protected $permLang=null;

    protected function init(){
        $app = ion\ion::get()->get_current_app();
        $this->options->json = json_decode(file_get_contents($app->DATA_ROOT.'translations.json'),true);

        $curLang = ion\ion::get()->curLang;
        $this->options->language = (ion\helpers::get()->validate($curLang)->is_null() === false) ? $curLang : 'en';
    }

    /**
     * @param string $phrase
     * @param bool|string $forceLang
     * @param array $replace
     * @return mixed
     */
    public function translate($phrase,$forceLang=false,$replace=[]){
        if($forceLang !== false && is_string($forceLang)) {
            $lang = $forceLang;
        }elseif($this->permLang !== null){
            $lang = $this->permLang;
        }else{
            $lang = $this->options->language;
        }
        $result = (array_key_exists($phrase,$this->options->json) && isset($this->options->json[$phrase][$lang])) ? $this->options->json[$phrase][$lang] : $phrase;

        if(count($replace) > 0){
            $result = vsprintf($result,$replace);
        }

        return $result;
    }

    public function set_language_permanent($lang){
        $this->permLang = $lang;
    }
}