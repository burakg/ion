<?php
/**
 *
 * Class numericHelpers
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 11.02.2015
 *
 */

namespace burakg\ion\helpers;

class numeric {
	use \burakg\ion\singletonTrait;

	public function set($val){
		$this->phrase = $val;
		return $this;
	}
}