<?php
/**
 *
 * Class htmlHelpers
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 11.02.2015
 *
 */

namespace burakg\ion\helpers;
use burakg\ion as ion;

class validate {
	use ion\singletonTrait;
	private $phrase;

	public function set($val){
		$this->phrase = $val;
		return $this;
	}

	public function is_null(){
		return in_array($this->phrase,array("",null,"null","NULL"));
	}

	public function is_numeric(){
		return (!empty($this->phrase) && is_numeric($this->phrase)) ? $this->phrase : 0;
	}

	public function is_array(){
		return (is_array($this->phrase) && count($this->phrase) > 0);
	}

	public function is_email(){
		return filter_var($this->phrase, FILTER_VALIDATE_EMAIL);
	}

	public function is_json() {
		$phrase = json_decode($this->phrase);
		return ($phrase !== NULL);
	}
}