<?php
/**
 *
 * Template class is used for shaping data into html outputs with regex matchings
 * Class template
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 11.02.2015
 *
 */

namespace burakg\ion\helpers;
use burakg\ion AS ion;

class template {
	use ion\singletonTrait;

	/**
	 * Initiates the templates
	 */
	protected function init(){
		$app = ion\ion::get()->get_current_app();
		$this->options->json = json_decode(file_get_contents($app->DATA_ROOT.'templates.json'));
	}

	/**
	 * Searches and replaces the template fields with given values
	 * @param $templateField
	 * @param $vars
	 * @return mixed|null
	 */
	public function apply($templateField,$vars){
		if(!is_array($vars) || count($vars) == 0 || ion\helpers\validate::get()->set($this->options->json->$templateField)->is_null() === true)
			return 'Template field and values are not provided or wrong template name';

		$output = $this->options->json->$templateField;
		foreach($vars AS $field => $value){
			$output = str_replace('{'.$field.'}',$value,$output);
		}

		return $output;
	}
}