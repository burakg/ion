<?php
/**
 *
 * Class textHelpers
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 11.02.2015
 *
 */

namespace burakg\ion\helpers;
use burakg\ion AS ion;
use Cocur\Slugify\Slugify;

class text {
	use ion\singletonTrait;

	public function set($val){
		$this->phrase = $val;
		return $this;
	}

	/**
	 * @param $size
	 * @param bool $ellipsis
	 * @return string
	 */
	public function limit($size,$ellipsis=true){
		$suffix = ($ellipsis === true) ? '...' : null;
		$text = ($size > 0 && strlen($this->phrase) > $size) ? substr($this->phrase,0,$size).$suffix : $this->phrase;
		return $text;
	}

    /**
     * @param bool $ignoreSuffix
     * @param array $options
     * @return string
     */
	public function slug($ignoreSuffix=false,$options=['separator' => '-','ruleset' => 'turkish']){
	    $slugger = new Slugify;
		$output = $slugger->slugify($this->phrase,$options);

		return ($ignoreSuffix === false && strlen($_POST['url-suffix']) > 0) ? $output.$_POST['url-suffix'] : $output;
	}

	/**
	 * @param string $subs
	 * @return string
	 */
	public function substitute($subs="-"){
		return ($this->phrase == '' || $this->phrase == null || $this->phrase === false || $this->phrase == "NULL" || $this->phrase == "null") ? $subs : $this->phrase;
	}

	/**
	 * @param string $subs
	 * @return string
	 */
	public function substitute_null($subs="-",$revert=false){
		if($revert === false){
			return ($this->phrase == '' || $this->phrase == null || $this->phrase === false || $this->phrase == "NULL" || $this->phrase == "null") ? $subs : null;
		}else{
			return ($this->phrase == '' || $this->phrase == null || $this->phrase === false || $this->phrase == "NULL" || $this->phrase == "null") ? null : $subs;
		}
	}

	/**
	 * @param string $sub1
	 * @param string $sub2
	 * @return string
	 */
	public function xor_filter($sub1,$sub2){
		return ($this->phrase == $sub1) ? $sub2 : $sub1;
	}
}