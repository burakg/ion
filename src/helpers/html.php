<?php
/**
 *
 * Class htmlHelpers
 * @author Burak Mehmet Gurbuz <burak@burakgurbuz.com>
 * http://burakgurbuz.com
 * Created on: 11.02.2015
 *
 */

namespace burakg\ion\helpers;
use burakg\ion AS ion;

class html {
	use ion\singletonTrait;
	private $phrase;

	public function set($val){
		$this->phrase = $val;
		return $this;
	}

	public function pre_print(){
		echo '<pre>',print_r($this->phrase,1),'</pre>';
	}

	public function show_image($path,$attributes=array(),$alternate=null,$tag=true,$forceCurrentApp=false){
		$attr = '';

		$targetApp = ($forceCurrentApp === false) ? (string)ion\ion::get()->get_current_app()->get_setting('managing') : null;
		$targetAppName = ion\helpers::get()->text($targetApp)->substitute(true);
		$srcFolder = ion\ion::get()->get_current_app($targetAppName)->CB_ASSETS.'uploads/';
		$frontFolder = ion\ion::get()->get_current_app($targetAppName)->CF_ASSETS.'uploads/';

		foreach($attributes AS $key => $value)
			$attr .= ' '.$key.'="'.$value.'"';

		if(file_exists($srcFolder.$path))
			return ($tag === true) ? '<img src="'.$frontFolder.$path.'"'.$attr.' />' : $frontFolder.$path;

		elseif($alternate != null)
			return ($tag === true) ? '<img src="'.$alternate.'"'.$attr.' />' : $alternate;

		else
			return null;
	}

	public function select_select($value,$data,$checkbox=false){
	    $val = ($checkbox === false) ? 'selected' : 'checked';
	    $output = null;
		if(!is_array($data)){
		    if($value == $data) $output = ' '.$val.'="'.$val.'"';
		}else{
            if(in_array($value, $data)) $output = ' '.$val.'="'.$val.'"';
		}

		return $output;
	}

    /**
     * @param $array
     * @param $current
     * @param string|array $titleField
     * @return string
     */
	public function data_list_to_select($array,$current,$titleField='title'){
		if(ion\helpers::get()->validate($array)->is_array()){
			array_walk($array,function(&$value,$key) use($titleField){
                if(is_array($titleField)){
                    $title = [];
                    foreach($titleField AS $t)
                        $title[] = $value[$t];

                    $value = [$value['id'],implode(' - ',$title)];
                }else{
    				$value = [$value['id'],$value[$titleField]];
                }
			});
			return $this->select_generate($array,$current);
		}else{
			return '<option disabled>'.$array.'</option>';
		}
	}

	public function select_generate($array,$current=0){
		$text = '';
		foreach($array AS $value){
			$selected = $this->select_select($value[0],$current);
			$disabled = ($value[2] === true) ? ' disabled="disabled"' : '';
			$text .= "		<option value=\"".$value[0]."\"".$selected.$disabled.">".$value[1]."</option>\n\r";
		}
		return "\n\r".$text;
	}

	public function nl2p($line_breaks = false){
        if ($line_breaks == true)
            return '<p>'.preg_replace(array("/([\n]{2,})/i", "/([^>])\n([^<])/i"), array("</p>\n<p>", '$1<br'.''.'>$2'), trim($this->phrase)).'</p>';
        else
            return '<p>'.preg_replace(
                array("/([\n]{2,})/i", "/([\r\n]{3,})/i","/([^>])\n([^<])/i"),
                array("</p>\n<p>", "</p>\n<p>", '$1<br'. ''.'>$2'),

                trim($this->phrase)).'</p>';
    }

	public function p2nl(){
        return trim(preg_replace(array("/<p[^>]*>/iU","/<\/p[^>]*>/iU"),array("","\n"),$this->phrase));
    }
}